# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160701175348) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "account_careers", force: :cascade do |t|
    t.boolean  "is_principal"
    t.integer  "account_id"
    t.integer  "career_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "account_languages", force: :cascade do |t|
    t.boolean  "is_principal"
    t.integer  "account_id"
    t.integer  "language_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "accounts", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "country_id"
    t.integer  "university_id"
    t.integer  "english_level"
    t.integer  "career_status"
    t.integer  "looking_for"
    t.decimal  "minimum_salary_expected",             precision: 12, scale: 2
    t.string   "resume",                  limit: 255
    t.string   "avatar",                  limit: 255
    t.datetime "created_at",                                                                   null: false
    t.datetime "updated_at",                                                                   null: false
    t.string   "step",                    limit: 255
    t.integer  "principal_career_id"
    t.integer  "old_country_id"
    t.integer  "years_experience"
    t.integer  "currency_id"
    t.string   "resume_tmp",              limit: 255
    t.boolean  "satander_user",                                                default: false
    t.integer  "email_notification",                                           default: 0
    t.integer  "account_state",                                                default: 0
    t.integer  "count_desactivated",                                           default: 0
    t.integer  "company_user_practice"
    t.string   "resumepractice"
    t.string   "report_card"
    t.string   "regular_student"
    t.string   "contract"
    t.integer  "user_score"
    t.boolean  "student_activities"
    t.boolean  "sport_activities"
    t.boolean  "student_ind_activities"
    t.boolean  "volunteering"
    t.boolean  "extra_activities"
    t.boolean  "student_projects"
  end

  create_table "accounts_careers", id: false, force: :cascade do |t|
    t.integer "account_id"
    t.integer "career_id"
  end

  create_table "accounts_certifications", id: false, force: :cascade do |t|
    t.integer "account_id"
    t.integer "certification_id"
  end

  add_index "accounts_certifications", ["account_id", "certification_id"], name: "index_accounts_certifications", using: :btree

  create_table "accounts_softwares", id: false, force: :cascade do |t|
    t.integer "account_id"
    t.integer "software_id"
  end

  add_index "accounts_softwares", ["account_id", "software_id"], name: "index_accounts_softwares_on_account_id_and_software_id", using: :btree

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "resource_id",   limit: 255, null: false
    t.string   "resource_type", limit: 255, null: false
    t.integer  "author_id"
    t.string   "author_type",   limit: 255
    t.text     "body"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "namespace",     limit: 255
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_admin_notes_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "answer_questionspractices", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_practice_id"
    t.integer  "answer"
    t.string   "text_answer"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "answers", force: :cascade do |t|
    t.integer  "question_id"
    t.text     "content"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "application_id"
  end

  create_table "applications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.datetime "deleted_at"
    t.boolean  "removed",         default: false
    t.integer  "salary_expected"
    t.boolean  "user_removed"
    t.integer  "company_user_id"
  end

  add_index "applications", ["company_user_id"], name: "index_applications_on_company_user_id", using: :btree
  add_index "applications", ["job_id"], name: "index_job_applications_on_job_post_id", using: :btree
  add_index "applications", ["user_id"], name: "index_job_applications_on_user_id", using: :btree

  create_table "area_job_practices", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "area_jobs", force: :cascade do |t|
    t.integer  "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "career_id"
    t.boolean  "same_area"
  end

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "company_id"
  end

  create_table "button_trackings", force: :cascade do |t|
    t.string   "button_type"
    t.string   "source"
    t.boolean  "removed"
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "button_trackings", ["job_id"], name: "index_button_trackings_on_job_id", using: :btree
  add_index "button_trackings", ["user_id"], name: "index_button_trackings_on_user_id", using: :btree

  create_table "candidates_request_processes", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "company_user_id"
    t.text     "comments"
    t.string   "state"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "candidates_request_processes", ["company_user_id"], name: "index_candidates_request_processes_on_company_user_id", using: :btree
  add_index "candidates_request_processes", ["job_id"], name: "index_candidates_request_processes_on_job_id", using: :btree

  create_table "careers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "country"
    t.integer  "area"
  end

  create_table "careers_jobs", id: false, force: :cascade do |t|
    t.integer "career_id"
    t.integer "job_id"
  end

  add_index "careers_jobs", ["career_id", "job_id"], name: "index_careers_jobs_on_career_id_and_job_id", using: :btree

  create_table "careers_post_requests", id: false, force: :cascade do |t|
    t.integer "career_id"
    t.integer "post_request_id"
  end

  add_index "careers_post_requests", ["career_id", "post_request_id"], name: "index_careers_post_requests_on_career_id_and_post_request_id", using: :btree

  create_table "certifications", force: :cascade do |t|
    t.string   "institution",        limit: 255
    t.string   "name",               limit: 255
    t.date     "certified_at"
    t.string   "certification_type", limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "comments_applications", force: :cascade do |t|
    t.integer  "application_id"
    t.integer  "company_user_id"
    t.text     "content"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "comments_applications", ["application_id"], name: "index_comments_applications_on_application_id", using: :btree
  add_index "comments_applications", ["company_user_id"], name: "index_comments_applications_on_company_user_id", using: :btree

  create_table "companies", force: :cascade do |t|
    t.string   "name",             limit: 255
    t.text     "description"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "video_source_url", limit: 255
    t.string   "avatar",           limit: 255
    t.string   "page_url",         limit: 255
    t.string   "slug",             limit: 255
    t.string   "public_image",     limit: 255
    t.string   "slider_image",     limit: 255
    t.integer  "company_status"
    t.integer  "founded"
    t.integer  "cant_employees"
    t.string   "industry",         limit: 255
    t.string   "central_office",   limit: 255
    t.string   "img_share_fb",     limit: 255
    t.integer  "number_eb"
    t.string   "square_image"
    t.string   "cover_image"
    t.integer  "country_id"
    t.integer  "bfje"
  end

  add_index "companies", ["country_id"], name: "index_companies_on_country_id", using: :btree

  create_table "company_users", force: :cascade do |t|
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.string   "email",                  limit: 255, default: "",    null: false
    t.string   "encrypted_password",     limit: 255, default: ""
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "company_id"
    t.integer  "hierarchy"
    t.string   "rut"
    t.integer  "plant"
    t.integer  "admin_type"
    t.boolean  "confirm_data",                       default: false
    t.date     "born_date"
    t.integer  "current_company_id"
  end

  add_index "company_users", ["confirmation_token"], name: "index_company_users_on_confirmation_token", unique: true, using: :btree
  add_index "company_users", ["email"], name: "index_company_users_on_email", unique: true, using: :btree
  add_index "company_users", ["reset_password_token"], name: "index_company_users_on_reset_password_token", unique: true, using: :btree

  create_table "countries", force: :cascade do |t|
    t.string   "name",         limit: 255
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "calling_code"
    t.string   "iso_code"
  end

  create_table "countries_post_requests", id: false, force: :cascade do |t|
    t.integer "country_id"
    t.integer "post_request_id"
  end

  add_index "countries_post_requests", ["country_id", "post_request_id"], name: "index_countries_post_requests_on_country_id_and_post_request_id", using: :btree

  create_table "currencies", force: :cascade do |t|
    t.integer  "country_id"
    t.string   "name",       limit: 255
    t.string   "symbol",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "documents", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "type_id"
    t.integer  "company_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "path"
  end

  add_index "documents", ["company_id"], name: "index_documents_on_company_id", using: :btree
  add_index "documents", ["user_id"], name: "index_documents_on_user_id", using: :btree

  create_table "download_files", force: :cascade do |t|
    t.integer  "user_id"
    t.datetime "expire_at"
    t.string   "token",      limit: 255
    t.string   "file_type",  limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "download_files", ["user_id"], name: "index_download_files_on_user_id", using: :btree

  create_table "encuesta_tutor_araucos", force: :cascade do |t|
    t.integer  "plant_id"
    t.integer  "area_id"
    t.string   "tutor_name"
    t.string   "tutor_position"
    t.integer  "task_orientation"
    t.integer  "responsability"
    t.integer  "tidy_details"
    t.integer  "proactivity"
    t.integer  "learning_capacity"
    t.integer  "communication_skills"
    t.integer  "relationships"
    t.integer  "teamwork"
    t.integer  "respect"
    t.boolean  "recommendable"
    t.text     "process_evaluation"
    t.text     "process_improvements"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "tolerance"
    t.integer  "company_user_id"
    t.integer  "user_id"
    t.integer  "final_calification"
    t.integer  "management_id"
    t.boolean  "is_complete"
    t.string   "user_code"
    t.boolean  "survey_extra"
    t.string   "user_fullname"
    t.string   "area_extra"
    t.string   "area_tutor"
  end

  add_index "encuesta_tutor_araucos", ["area_id"], name: "index_encuesta_tutor_araucos_on_area_id", using: :btree
  add_index "encuesta_tutor_araucos", ["company_user_id"], name: "index_encuesta_tutor_araucos_on_company_user_id", using: :btree
  add_index "encuesta_tutor_araucos", ["management_id"], name: "index_encuesta_tutor_araucos_on_management_id", using: :btree
  add_index "encuesta_tutor_araucos", ["plant_id"], name: "index_encuesta_tutor_araucos_on_plant_id", using: :btree
  add_index "encuesta_tutor_araucos", ["user_id"], name: "index_encuesta_tutor_araucos_on_user_id", using: :btree

  create_table "endowments", force: :cascade do |t|
    t.integer  "company_user_id"
    t.integer  "management_id"
    t.integer  "plant_id"
    t.integer  "location_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "management_second_id"
  end

  create_table "evaluations", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "task_orientation"
    t.integer  "responsability"
    t.integer  "tidy_details"
    t.integer  "proactivity"
    t.integer  "communication_skills"
    t.integer  "relationships"
    t.integer  "teamwork"
    t.text     "comment"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "summon_id"
    t.boolean  "is_recommendable"
    t.integer  "company_user_id"
  end

  add_index "evaluations", ["company_user_id"], name: "index_evaluations_on_company_user_id", using: :btree
  add_index "evaluations", ["summon_id"], name: "index_evaluations_on_summon_id", using: :btree
  add_index "evaluations", ["user_id"], name: "index_evaluations_on_user_id", using: :btree

  create_table "excels", force: :cascade do |t|
    t.integer  "account_id"
    t.integer  "level"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "removed"
    t.string   "source"
  end

  add_index "favorites", ["job_id"], name: "index_favorites_on_job_id", using: :btree
  add_index "favorites", ["user_id"], name: "index_favorites_on_user_id", using: :btree

  create_table "get_more_tracking_anons", force: :cascade do |t|
    t.integer  "count"
    t.integer  "job_id"
    t.string   "source"
    t.string   "source_utm"
    t.string   "public_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "get_more_tracking_anons", ["job_id"], name: "index_get_more_tracking_anons_on_job_id", using: :btree

  create_table "get_more_trackings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.integer  "count"
    t.string   "source"
    t.string   "source_utm"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "get_more_trackings", ["job_id"], name: "index_get_more_trackings_on_job_id", using: :btree
  add_index "get_more_trackings", ["user_id"], name: "index_get_more_trackings_on_user_id", using: :btree

  create_table "interview_practices", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "interview_type"
    t.integer  "vacancies"
    t.datetime "interview_start"
    t.datetime "interview_end"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "company_user_id"
  end

  add_index "interview_practices", ["company_user_id"], name: "index_interview_practices_on_company_user_id", using: :btree

  create_table "interviews", force: :cascade do |t|
    t.date     "starts_at"
    t.string   "location",        limit: 255
    t.string   "state",           limit: 255
    t.integer  "job_id"
    t.integer  "company_user_id"
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "job_expansions", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "number_vacancies"
    t.date     "start_date"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.text     "function1"
    t.text     "function2"
    t.text     "function3"
    t.integer  "status_job"
    t.text     "comment_rejected"
    t.integer  "responsable_company_user_id"
    t.integer  "location_id"
    t.integer  "company_job",                 default: 1
  end

  create_table "job_statuscareers", force: :cascade do |t|
    t.integer  "job_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "student",    default: false
    t.boolean  "graduate",   default: false
    t.boolean  "qualified",  default: false
  end

  create_table "jobs", force: :cascade do |t|
    t.integer  "company_id"
    t.string   "title",                     limit: 255
    t.text     "description"
    t.float    "maximum_salary_offered"
    t.boolean  "show_salary"
    t.integer  "posted_by_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.string   "location",                  limit: 255
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "english_level"
    t.integer  "career_status"
    t.integer  "looking_for"
    t.string   "slug",                      limit: 255
    t.integer  "currency_id"
    t.boolean  "published",                             default: true
    t.string   "avatar",                    limit: 255
    t.string   "countries",                 limit: 255, default: "t"
    t.integer  "excel_level"
    t.text     "activities"
    t.integer  "hierarchy_job"
    t.integer  "years_experience"
    t.integer  "state"
    t.integer  "vacancies"
    t.integer  "availability"
    t.integer  "filter_califications"
    t.integer  "filter_assistantship"
    t.integer  "filter_extra_activities"
    t.integer  "filter_student_activities"
    t.integer  "filter_student_project"
    t.integer  "age_study"
  end

  add_index "jobs", ["company_id"], name: "index_job_posts_on_company_id", using: :btree
  add_index "jobs", ["posted_by_id"], name: "index_job_posts_on_posted_by_id", using: :btree
  add_index "jobs", ["slug"], name: "index_jobs_on_slug", unique: true, using: :btree

  create_table "jobs_countries", id: false, force: :cascade do |t|
    t.integer "job_id"
    t.integer "country_id"
  end

  add_index "jobs_countries", ["job_id", "country_id"], name: "index_jobs_countries_on_job_id_and_country_id", using: :btree

  create_table "jobs_universities", id: false, force: :cascade do |t|
    t.integer "job_id"
    t.integer "university_id"
  end

  add_index "jobs_universities", ["job_id", "university_id"], name: "index_jobs_universities_on_job_id_and_university_id", using: :btree

  create_table "languages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "level"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "last_views", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "company_user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "last_views", ["company_user_id"], name: "index_last_views_on_company_user_id", using: :btree
  add_index "last_views", ["job_id"], name: "index_last_views_on_job_id", using: :btree

  create_table "like_applications", force: :cascade do |t|
    t.integer  "application_id"
    t.integer  "company_user_id"
    t.boolean  "liked"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "like_applications", ["application_id"], name: "index_like_applications_on_application_id", using: :btree
  add_index "like_applications", ["company_user_id"], name: "index_like_applications_on_company_user_id", using: :btree

  create_table "location_jobs", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "location_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "plant_id"
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "region_id"
  end

  create_table "managements", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "managment_seconds", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "matchings", force: :cascade do |t|
    t.boolean  "is_suggested"
    t.string   "suggested_by_id", limit: 255
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.boolean  "removed"
  end

  add_index "matchings", ["job_id"], name: "index_matchings_on_job_id", using: :btree
  add_index "matchings", ["user_id"], name: "index_matchings_on_user_id", using: :btree

  create_table "newdatausers", force: :cascade do |t|
    t.integer  "account_id"
    t.string   "rut",                  limit: 255
    t.integer  "age"
    t.string   "phone",                limit: 255
    t.integer  "age_study"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "address"
    t.integer  "region_id"
    t.float    "qualifications"
    t.integer  "time_practice"
    t.integer  "assistantship",                    default: 0, null: false
    t.integer  "assistantship_number"
    t.datetime "date_practice"
    t.integer  "user_score"
    t.string   "sex"
    t.string   "nationality"
  end

  create_table "nopreferences", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.integer  "type_notification"
    t.boolean  "viewed",            default: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "plants", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "company_id", default: 2
  end

  create_table "post_requests", force: :cascade do |t|
    t.string   "company_name",           limit: 255
    t.string   "name",                   limit: 255
    t.string   "position",               limit: 255
    t.string   "email",                  limit: 255
    t.string   "phone",                  limit: 255
    t.integer  "looking_for"
    t.integer  "career_status"
    t.string   "location",               limit: 255
    t.float    "maximum_salary_offered"
    t.boolean  "show_salary",                        default: false
    t.integer  "currency_id"
    t.integer  "english_level"
    t.string   "title",                  limit: 255
    t.text     "description"
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "company_type"
    t.integer  "vacancies"
  end

  add_index "post_requests", ["currency_id"], name: "index_post_requests_on_currency_id", using: :btree

  create_table "post_requests_universities", id: false, force: :cascade do |t|
    t.integer "post_request_id"
    t.integer "university_id"
  end

  create_table "preference_practices", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "question_practices", force: :cascade do |t|
    t.string   "question"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "type_question"
    t.integer  "company_id"
  end

  create_table "question_video_interviews", force: :cascade do |t|
    t.string   "content",            limit: 255
    t.integer  "video_interview_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "questions", force: :cascade do |t|
    t.integer  "job_id"
    t.string   "content",         limit: 255
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "post_request_id"
  end

  create_table "referreds", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.integer  "job_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "refered_by"
    t.integer  "company_id",  default: 2
    t.integer  "type_id"
    t.string   "last_name"
    t.datetime "notified_at"
  end

  create_table "regions", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "removed_jobs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean  "removed"
    t.string   "source"
  end

  add_index "removed_jobs", ["job_id"], name: "index_removed_jobs_on_job_id", using: :btree
  add_index "removed_jobs", ["user_id"], name: "index_removed_jobs_on_user_id", using: :btree

  create_table "request_applicants", force: :cascade do |t|
    t.integer  "company_id"
    t.integer  "company_user_id"
    t.integer  "job_id"
    t.text     "comments"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.datetime "read_at"
  end

  add_index "request_applicants", ["company_id"], name: "index_request_applicants_on_company_id", using: :btree
  add_index "request_applicants", ["company_user_id"], name: "index_request_applicants_on_company_user_id", using: :btree
  add_index "request_applicants", ["job_id"], name: "index_request_applicants_on_job_id", using: :btree

  create_table "reviews", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "age"
    t.string   "profession", limit: 255
    t.text     "comment"
    t.string   "avatar",     limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "kind"
  end

  create_table "share_trackings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.string   "source_utm"
    t.string   "network"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "share_trackings", ["job_id"], name: "index_share_trackings_on_job_id", using: :btree
  add_index "share_trackings", ["user_id"], name: "index_share_trackings_on_user_id", using: :btree

  create_table "show_more_tracking_anons", force: :cascade do |t|
    t.integer  "count"
    t.integer  "job_id"
    t.string   "source"
    t.string   "source_utm"
    t.string   "public_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "show_more_tracking_anons", ["job_id"], name: "index_show_more_tracking_anons_on_job_id", using: :btree

  create_table "show_more_trackings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "job_id"
    t.integer  "count"
    t.string   "source"
    t.string   "source_utm"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "show_more_trackings", ["job_id"], name: "index_show_more_trackings_on_job_id", using: :btree
  add_index "show_more_trackings", ["user_id"], name: "index_show_more_trackings_on_user_id", using: :btree

  create_table "softwares", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "level"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "summons", force: :cascade do |t|
    t.integer  "job_id"
    t.integer  "user_id"
    t.integer  "interview_practice_id"
    t.datetime "confirmation_send_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "user_response"
    t.text     "user_comment"
    t.integer  "recommendable"
    t.boolean  "selected"
    t.boolean  "confirmed"
  end

  create_table "survey_user_araucos", force: :cascade do |t|
    t.integer  "management_id"
    t.integer  "job_place"
    t.integer  "practice_duration"
    t.integer  "year_study_end"
    t.boolean  "know_company"
    t.integer  "how_know_company"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "professionalism"
    t.integer  "learning"
    t.integer  "expectations"
    t.integer  "inducement"
    t.integer  "feedback"
    t.integer  "resources"
    t.integer  "tutor_relationship"
    t.integer  "security_plant"
    t.integer  "innovation_company"
    t.integer  "work_environment"
    t.integer  "user_id"
    t.integer  "trained_by"
    t.integer  "priority_1"
    t.integer  "priority_2"
    t.boolean  "work_in_arauco"
    t.integer  "salary"
    t.text     "comments"
    t.integer  "job_type"
    t.boolean  "is_complete"
  end

  add_index "survey_user_araucos", ["management_id"], name: "index_survey_user_araucos_on_management_id", using: :btree
  add_index "survey_user_araucos", ["user_id"], name: "index_survey_user_araucos_on_user_id", using: :btree

  create_table "universities", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "country_id"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.date     "born_date"
    t.string   "slug",                   limit: 255
    t.string   "authentication_token"
    t.string   "public_token"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

  create_table "video_interviews", force: :cascade do |t|
    t.integer  "job_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "user_id"
    t.integer  "company_user_id"
    t.string   "state",           limit: 255
    t.boolean  "viewed",                      default: false, null: false
    t.string   "link",            limit: 255
  end

  create_table "where_hears", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "hear"
    t.string   "detail"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "detail_where"
    t.integer  "company_id"
  end

  create_table "zones", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "city_id"
    t.integer  "accommodation"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_foreign_key "applications", "company_users"
  add_foreign_key "button_trackings", "jobs"
  add_foreign_key "button_trackings", "users"
  add_foreign_key "candidates_request_processes", "company_users"
  add_foreign_key "candidates_request_processes", "jobs"
  add_foreign_key "comments_applications", "applications"
  add_foreign_key "comments_applications", "company_users"
  add_foreign_key "companies", "countries"
  add_foreign_key "documents", "companies"
  add_foreign_key "documents", "users"
  add_foreign_key "encuesta_tutor_araucos", "areas"
  add_foreign_key "encuesta_tutor_araucos", "company_users"
  add_foreign_key "encuesta_tutor_araucos", "managements"
  add_foreign_key "encuesta_tutor_araucos", "plants"
  add_foreign_key "encuesta_tutor_araucos", "users"
  add_foreign_key "evaluations", "company_users"
  add_foreign_key "evaluations", "summons"
  add_foreign_key "evaluations", "users"
  add_foreign_key "favorites", "jobs"
  add_foreign_key "favorites", "users"
  add_foreign_key "get_more_tracking_anons", "jobs"
  add_foreign_key "get_more_trackings", "jobs"
  add_foreign_key "get_more_trackings", "users"
  add_foreign_key "interview_practices", "company_users"
  add_foreign_key "last_views", "company_users"
  add_foreign_key "last_views", "jobs"
  add_foreign_key "like_applications", "applications"
  add_foreign_key "like_applications", "company_users"
  add_foreign_key "removed_jobs", "jobs"
  add_foreign_key "removed_jobs", "users"
  add_foreign_key "request_applicants", "companies"
  add_foreign_key "request_applicants", "company_users"
  add_foreign_key "request_applicants", "jobs"
  add_foreign_key "share_trackings", "jobs"
  add_foreign_key "share_trackings", "users"
  add_foreign_key "show_more_tracking_anons", "jobs"
  add_foreign_key "show_more_trackings", "jobs"
  add_foreign_key "show_more_trackings", "users"
  add_foreign_key "survey_user_araucos", "managements"
  add_foreign_key "survey_user_araucos", "users"
end
