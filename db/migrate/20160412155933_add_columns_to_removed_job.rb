class AddColumnsToRemovedJob < ActiveRecord::Migration
  def change
    add_column :removed_jobs, :removed, :boolean
    add_column :removed_jobs, :source, :string
  end
end
