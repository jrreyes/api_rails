class AddCurrentCompanyIdToCompanyUser < ActiveRecord::Migration
  def change
    add_column :company_users, :current_company_id, :integer
  end
end
