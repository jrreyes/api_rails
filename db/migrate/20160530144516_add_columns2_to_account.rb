class AddColumns2ToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :extra_activities, :boolean
    add_column :accounts, :student_projects, :boolean
  end
end
