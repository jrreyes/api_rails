class CreateShareTrackings < ActiveRecord::Migration
  def change
    create_table :share_trackings do |t|
      t.references :user, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true
      t.string :source_utm
      t.string :network

      t.timestamps null: false
    end
  end
end
