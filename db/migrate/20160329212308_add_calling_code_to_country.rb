class AddCallingCodeToCountry < ActiveRecord::Migration
  def change
    add_column :countries, :calling_code, :string
  end
end
