class AddColumnssToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :student_activities, :boolean
    add_column :accounts, :sport_activities, :boolean
    add_column :accounts, :student_ind_activities, :boolean
    add_column :accounts, :volunteering, :boolean
  end
end
