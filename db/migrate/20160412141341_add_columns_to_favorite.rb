class AddColumnsToFavorite < ActiveRecord::Migration
  def change
    add_column :favorites, :removed, :boolean
    add_column :favorites, :source, :string
  end
end
