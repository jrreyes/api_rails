class CreateCandidatesRequestProcesses < ActiveRecord::Migration
  def change
    create_table :candidates_request_processes do |t|
      t.references :job, index: true, foreign_key: true
      t.references :company_user, index: true, foreign_key: true
      t.text :comments
      t.string :state

      t.timestamps null: false
    end
  end
end
