class CreateGetMoreTrackings < ActiveRecord::Migration
  def change
    create_table :get_more_trackings do |t|
      t.references :user, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true
      t.integer :count
      t.string :source
      t.string :source_utm

      t.timestamps null: false
    end
  end
end
