class CreateCommentsApplications < ActiveRecord::Migration
  def change
    create_table :comments_applications do |t|
      t.references :application, index: true, foreign_key: true
      t.references :company_user, index: true, foreign_key: true
      t.text :content

      t.timestamps null: false
    end
  end
end
