class AddUserRemovedToApplication < ActiveRecord::Migration
  def change
    add_column :applications, :user_removed, :boolean
  end
end
