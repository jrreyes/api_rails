class CreateShowMoreTrackingAnons < ActiveRecord::Migration
  def change
    create_table :show_more_tracking_anons do |t|
      t.integer :count
      t.references :job, index: true, foreign_key: true
      t.string :source
      t.string :source_utm
      t.string :public_token

      t.timestamps null: false
    end
  end
end
