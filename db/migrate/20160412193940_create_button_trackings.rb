class CreateButtonTrackings < ActiveRecord::Migration
  def change
    create_table :button_trackings do |t|
      t.string :button_type
      t.string :source
      t.boolean :removed
      t.references :user, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
