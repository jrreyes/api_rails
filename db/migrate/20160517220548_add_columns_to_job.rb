class AddColumnsToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :filter_califications, :integer
    add_column :jobs, :filter_assistantship, :integer
    add_column :jobs, :filter_extra_activities, :integer
    add_column :jobs, :filter_student_activities, :integer
    add_column :jobs, :filter_student_project, :integer
  end
end
