class AddAvailabilityToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :availability, :integer
  end
end
