class AddColumnsToReferred < ActiveRecord::Migration
  def change
    add_column :referreds, :last_name, :string
    add_column :referreds, :notified_at, :datetime
  end
end
