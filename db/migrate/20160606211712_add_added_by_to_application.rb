class AddAddedByToApplication < ActiveRecord::Migration
  def change
    add_reference :applications, :company_user, index: true, foreign_key: true
  end
end
