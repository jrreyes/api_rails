class AddRemovedToMatching < ActiveRecord::Migration
  def change
    add_column :matchings, :removed, :boolean
  end
end
