class CreateRequestApplicants < ActiveRecord::Migration
  def change
    create_table :request_applicants do |t|
      t.references :company, index: true, foreign_key: true
      t.references :company_user, index: true, foreign_key: true
      t.references :job, index: true, foreign_key: true
      t.text :comments

      t.timestamps null: false
    end
  end
end
