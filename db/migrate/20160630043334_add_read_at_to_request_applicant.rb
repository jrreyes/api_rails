class AddReadAtToRequestApplicant < ActiveRecord::Migration
  def change
    add_column :request_applicants, :read_at, :datetime
  end
end
