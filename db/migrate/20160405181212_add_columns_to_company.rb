class AddColumnsToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :square_image, :string
    add_column :companies, :cover_image, :string
  end
end
