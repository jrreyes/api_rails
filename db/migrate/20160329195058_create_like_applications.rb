class CreateLikeApplications < ActiveRecord::Migration
  def change
    create_table :like_applications do |t|
      t.references :application, index: true, foreign_key: true
      t.references :company_user, index: true, foreign_key: true
      t.boolean :liked

      t.timestamps null: false
    end
  end
end
