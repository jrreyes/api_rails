class MatchWorker
  # Set the Queue as Default
  # queue_as :default
  include Sidekiq::Worker
  def perform(newer_than, job_id, user_id)
    job = Job.find(job_id)
    matchings = Matching.select(:id).where(job_id: job.id)
    matchings.update_all(removed: true)
    accounts = Account.matchings_accounts(job.id).where("accounts.updated_at >= ?", newer_than.months.ago)
    accounts = Account.where(user_id: user_id) if user_id.present?

    accounts.each do |account|
      match_ = Matching.where(user_id: account.user_id, job_id: job.id)
      if match_.exists?
        match = match_.last
      else
        match = Matching.new(is_suggested: false, user_id: account.user_id, job_id: job.id)
      end
        match.removed = false # siginifica que es match
        match.save
      end
    end
  end