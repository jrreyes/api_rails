module ApplicationHelper

  def is_selected?(a,b)
    "selected" if a.to_s == b.to_s
  end

  def is_selected_multi?(data, value)
    return "" if value.nil? || value.blank? || data.nil? || data.blank?
    "selected" if data.include?(value.to_s) || data.include?(value)
  end

  def selected2?(values, value)
    return "" if values.nil?
    return "selected" if values.include?(value.to_s)
    ""
  end

  def is_checked?(a,b)
    "checked" if a.to_s == b.to_s
  end

  def is_checked2?(values, value)
    return "" if values.nil?
    return "checked" if values.include?(value.to_s)
    ""
  end

  def is_active?(a,b)
    "active" if a.to_s == b.to_s
  end

  def is_active2?(values, value)
    return "" if values.nil?
    return "active" if values.include?(value.to_s)
    ""
  end

  def panel_state(state)
    return "panel-gray" if state == 1
    return "panel-green" if state == 2
    return "panel-orange" if state == 3
    return "panel-red" if state == 4
    ""
  end

  def is_disabled?(state)
    return "disabled" if state == 1
  end

  def is_pendent?(state)
    return true if state == 1
  end

  def is_close?(state)
    return "disabled" if state == 4
  end

  def is_paused(state)
    return "disabled" if state == 3
  end

  def step1_route(job)
    return client_create_process_path if job.id.nil?
    client_process_step_1_update_path
  end
end
