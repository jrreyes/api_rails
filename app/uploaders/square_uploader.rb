class SquareUploader < CarrierWave::Uploader::Base

  include CarrierWave::RMagick
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  def default_url
    # For Rails 3.1+ asset pipeline compatibility:
    # asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
    #ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
    return "https://placehold.it/300x300"
  end

  version :big_thumb do
    process resize_to_fit: [100, 100]
  end

  # if image is not 300x300 , do this: 
  #version :box do
  #  process resize_to_fit: [300, 300]
  #end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

end