class DataController < ApplicationController

  def get_countries
    countries = Country.all.select(:id, :name, :calling_code, :iso_code)
    render json: countries, status: 200
  end

  def get_careers
    render json: Career.all.select(:id, :name, :country), status: 200
  end

  def get_careers_by_country
    country_id = params[:country_id]
    render json: Career.by_country(country_id), status: 200
  end

  def get_universities
    render json: University.all.select(:id, :name, :country_id), status: 200
  end

  def get_universities_by_country
    country_id = params[:country_id]
    render json: University.by_country(country_id), status: 200
  end

  def get_questions
    type_question = params[:type_question]
    questions = type_question.present? ? QuestionPractice.where(type_question: type_question) : QuestionPractice.all
    render json: questions, status: 200
  end

end