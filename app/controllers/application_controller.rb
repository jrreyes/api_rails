class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :authenticate_user_from_token! # For API REST auth
  before_action :validate_job_id_param

  def is_company_user?
  	if !company_user_signed_in?
  		respond_to do |format|
  			format.html {render file: 'public/404', status: 401}
  			format.json {render json: "401: Acceso restringido", status: 401}
  		end
  		#render file: 'public/404', status: 404, formats: [:html, :json]
  		return
  	end
  end

  def authenticate_user_from_token!
    return if ( request.headers["User-Email"].nil? || request.headers["User-Token"].nil? )
    user_email = request.headers["User-Email"].presence
    user = user_email && User.find_by_email(user_email)
    if user
      request.env['devise.skip_trackable'] = true
      if user && Devise.secure_compare(user.authentication_token, request.headers["User-Token"])
        sign_in user, store: false
      else
        render :json=> {:message=>"Error with your login or password"}, :status=> 401
      end
    end
  end

  def authenticate!
    current_admin_user || current_user || current_company_user || authenticate_company_user!
  end

  def auth_company_user!
    #return render json: {message: "You are not allowed to perform this action.", status: 401}, status: 401 if current_company_user.nil?
    redirect_to :client_login if current_company_user.nil?
  end

  def auth_user!
    return render json: {message: "You are not allowed to perform this action.", status: 401}, status: 401 if current_user.nil?
  end

  def find_job_by_params(job_id)
    return nil if job_id.nil?
    return render json: {message: "Error: Job not found"}, status: 404 unless Job.find(job_id)
    Job.find(job_id)
  end

  def validate_job_id_param
    job_id = params[:job_id]
    return if job_id.nil?
    return render json: {message: "Error: Job not found"}, status: 404 unless Job.find(job_id)
  end

end