class Client::InsigthsController < ApplicationController

  def index
    @jobs = Job.select("id").get_all_jobs_from_company current_company_user, "todos", "created_at"
    @accounts = Account.select("id").search_account_from_process(current_company_user)
  end
  
end