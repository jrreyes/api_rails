class Client::CompanyController < ApplicationController

  def index
    render layout: "companies"
  end

  def login
    redirect_to client_process_index_path if current_company_user.present?
  end

  def select_company
      @companies = Company.all.order(name: :asc)
  end

  def set_company_for_kam
    company_id = params[:current_company_id]
    current_company_user.update_attribute(:current_company_id, company_id)
    redirect_to client_process_index_path
  end

  def products
    @page_title = "Productos"
    render layout: "companies"
  end

  def plans
    @page_title = "Nuestros planes"
    render layout: "companies"
  end

  def publish
    @page_title = "Publica un trabajo"
    render layout: "companies"
  end

  def clients
    @page_title = "Clientes"
    render layout: "companies"
  end

  def contact
    @page_title = "Contacto"
    @reviews = Review.company.sample(2)
    render layout: "companies"
  end

  def post_requests
    params.permit!
    @post_request = PostRequest.new(params[:post_request])
    if @post_request.save
      flash[:notice] = "Successfully created post_request."
      Notifier.new_post_request(@post_request).deliver_now
      render js: "publish_ok();"
    else
      render js: "alert('Favor intentar nuevamente.');"
    end

  end

  def create
    @message = Message.new(params[:message])
    if @message.valid?
      Notifier.new_message(@message).deliver_now
      render js: 'contact_ok();'
    else
      render js: 'contact_error();'
    end
  end

end