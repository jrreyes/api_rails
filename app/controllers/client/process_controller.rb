class Client::ProcessController < ApplicationController

  before_action :auth_company_user!, except: [:download_resume] # Agregar auth para company_user!
  before_action :set_application, only: [:get_application_answers, :get_application_comments]
  protect_from_forgery :except => :get_job_resumes

  require 'action_view/helpers/javascript_helper'
  include ActionView::Helpers::JavaScriptHelper
  require 'zip'

  def index
    looking_for = params[:looking_for]
    order_by = params[:order_by].present? ? params[:order_by] : "created_at"
    @jobs = Job.get_all_jobs_from_company(current_company_user, looking_for, order_by)
    @jobs = @jobs.paginate(:page => params[:page])
  end

  def show
    @job = Job.find(params[:job_id])
    @applications = Application.get_applicants_all(@job.id)
    @accounts = Account.where(user_id: @applications.map(&:user_id))
    @accounts = Account.filters(@accounts, params, @job.id) if params.present?
    LastView.set_last_view(@job.id, current_company_user.id)
    @accounts = @accounts.paginate(:page => params[:page])
  end

  def republish
    @job = Job.find(params[:job_id])
    @job_clone = Job.new
    @job_clone.activities = @job.activities
    @job_clone.avatar = @job.avatar
    @job_clone.career_status = @job.career_status
    @job_clone.careers = @job.careers
    @job_clone.universities = @job.universities
    @job_clone.company_id = @job.company_id
    @job_clone.countries = @job.countries
    @job_clone.currency_id = @job.currency_id
    @job_clone.description = @job.description
    @job_clone.english_level = @job.english_level
    @job_clone.excel_level = @job.excel_level
    @job_clone.hierarchy_job = @job.hierarchy_job
    @job_clone.job_statuscareer = @job.job_statuscareer
    @job_clone.latitude = @job.latitude
    @job_clone.longitude = @job.longitude
    @job_clone.location = @job.location
    @job_clone.looking_for = @job.looking_for
    @job_clone.maximum_salary_offered = @job.maximum_salary_offered
    @job_clone.posted_by_id = @job.posted_by_id
    @job_clone.job_statuscareer = @job.job_statuscareer
    @job_clone.published = false
    @job_clone.show_salary = @job.show_salary
    @job_clone.title = @job.title.to_s + " - 2"
    @job_clone.years_experience = @job.years_experience
    @job_clone.job_expansion = @job.job_expansion
    @job_clone.questions = @job.questions
    @job_clone.state = 1

    if @job_clone.save
      # Al crear la copia, redireccionar a editar proceso nuevo.
      job_url = client_edit_step_1_url(@job_clone.id)
      render js: "window.location.href = '#{job_url}';", status: 200
    else
      render js: "swal('Ha ocurrido un error al republicar.');"
    end
  end

  def get_application_answers
    render json: @application.get_answers, status: 200
  end

  def set_job_state
    @job = find_job_by_params(params[:job_id])
    state = params[:state]    
    if @job.set_state(state)
      buttons_html = escape_javascript render_to_string "client/process/_job_buttons", :layout => false
      return render js: "$('.buttons-job-#{@job.id}').html('#{buttons_html}');" , status: 200
    end
    render json: {message: "error: " + @job.errors.full_messages.to_s}, status: 500
  end

  def request_candidates
    job = find_job_by_params(params[:job_id])
    comments = params[:comments]
    ClientMailer.request_new_candidate(current_company_user, job, comments).deliver_now
    new_request = RequestApplicant.set_data(comments, job, current_company_user)
    return render js: "request_new_candidate('#{job.id}');", status: 200 if new_request.save
    render json: {message: new_request.errors.full_messages.to_s}, status: 500
  end

  def get_job_details
    job = find_job_by_params(params[:job_id])
    render json: Job.get_job_details(job.id), status: 200
  end

  def get_application_comments
    render json: @application.get_comments, status: 200
  end

  #def get_all_jobs_from_company
  #  data = []
  #  jobs = Job.get_all_jobs_from_company(current_company_user)
  #  jobs.each do |job|
  #    data.push(Job.get_job_details(job.id))
  #  end
  #  render json: data, status: 200
  #end

  def add_applicant
    email = params[:email]
    first_name = params[:first_name]
    last_name = params[:last_name]
    job_id = params[:job_id]
    send_email = params[:send_email]
    resp = Application.add_applicant(current_company_user, email, first_name, last_name, job_id, send_email)
    return render js: "alert('ok');", status: 200 if resp
    render js: "alert('not found');", status: 404
  end

  def get_applicants_for_job
    job = find_job_by_params(params[:job_id])
    page = params[:page]
    applicants = Application.get_applicants_in_job(job.id).paginate(:page => page)
    data = {
      users: User.get_users_info(applicants.map(&:user_id))
    }
    render json: data, status: 200
  end

  def get_user_resume
    @user = User.find(params[:user_id])
    if @user.account.resume.present?
      filename = @user.account.resume.path
      s3 = Aws::S3::Client.new(region:'sa-east-1')
      aws_object = s3.get_object(bucket:'firstjobcvs', key: filename).body.read
      send_data(aws_object, :filename => filename, :disposition => 'attachment')
    end
  end

  def display_resume
    @user = User.find(params[:user_id])
    render partial: "partials/display_resume.html.erb"
  end

  def add_new_referred
    first_name = params[:first_name]
    last_name = params[:last_name]
    email = params[:email]
    notify = params[:notify]
    job_id = params[:job_id]
    referred = Referred.add_new_referred(current_company_user, first_name, last_name, email, job_id, notify)
    return render json: {message: "ok"}, status: 200 if referred
    render json: {message: "error"}, status: 500
  end

  def likes_buttons
    button = params[:button_type]
    application_id = params[:application_id]
    current_value = params[:current_value].to_s
    liked = LikeApplication.likes_click(button, current_company_user, application_id, current_value)
    render js: "setLikesValue(#{application_id}, '#{liked}');", status: 200    
  end

  def new_application_comment
    content = params[:content]
    app_id = params[:app_id]
    @app = Application.find(app_id)
    CommentsApplication.new_comment(content, app_id, current_company_user.id)
    html_content = escape_javascript render_to_string "client/process/_user_comments", :layout => false
    render js: "$('.pane-comentarios-#{@app.user_id}').html('#{html_content}');", status: 200
  end

  def edit_application_comment
    comment_id = params[:comment_id]
    content = params[:content]
    comment = CommentsApplication.find(comment_id)
    comment.content = content
    @app = comment.application
    comment.save if comment.company_user.id == current_company_user.id
    html_content = escape_javascript render_to_string "client/process/_user_comments", :layout => false
    render js: "$('.pane-comentarios-#{@app.user_id}').html('#{html_content}');", status: 200
    # render js: "edit_application_comment(#{user_id}, #{comment_id}, '#{comment.content}');", status: 200
    # render js: "alert('#{comment_id}');", status: 200
  end

  def delete_application_comment
    comment_id = params[:comment_id]
    comment = CommentsApplication.find(comment_id)
    @app = comment.application
    comment.destroy if comment.company_user.id == current_company_user.id
    html_content = escape_javascript render_to_string "client/process/_user_comments", :layout => false
    render js: "$('.pane-comentarios-#{@app.user_id}').html('#{html_content}');", status: 200
  end

  def get_profile_partial
    app = Application.find(params[:app_id])
    render partial: "client/process/user_profile", status: 200
  end

  def get_process_partial
    app = Application.find(params[:app_id])
    user = app.user
    @applications = user.get_applications
    render partial: "client/process/user_process", status: 200
  end

  def get_answers_partial
    app = Application.find(params[:app_id])
    @questions = app.get_answers
    render partial: "client/process/user_answers", status: 200
  end

  def get_comments_partial
    @app = Application.find(params[:app_id])
    render partial: "client/process/user_comments", status: 200
  end

  def getexcel
    @job = Job.find(params[:job_id])
    @file = "#{Rails.root}/public/basic.xlsx"
    #@user_ids     = @job.interviews.map(&:user_id)
    #@matched_ids  = @job.matchings.map(&:user_id)
    root = root_url(:only_path => false)
    scope = params[:scope]
    exist_profiles = false

    # @applications = @job.applications.not_accepted(@user_ids + @matched_ids).not_removed
    @applications = Application.get_applicants_all(@job.id)

    @applications = Application.where(id: params[:application_ids]) if params[:application_ids].present?
    #@applications = @job.applications.not_accepted(@user_ids).by_user(@matched_ids).not_removed

    @questions = Question.where(job_id: @job.id)
    excel_file = create_excel_file()
    excel_file.serialize(@file)

    if @applications.present?
      exist_profiles = true
    end
    if scope == "all" || scope.blank?
      if exist_profiles
        return render text: '<meta http-equiv="refresh" content="0; url=' + "/basic.xlsx" +  '" />' if params[:application_ids].present?
        send_file(@file, filename: ""+@job.title+"_todos.xlsx", type: "application/vnd.ms-excel")
      else
        flash[:notice] = "No hay candidatos en esta tabla"
        redirect_to :back 
      end
    elsif scope == "matched"
      if exist_profiles
        send_file(@file, filename: ""+@job.title+"_matchs.xlsx", type: "application/vnd.ms-excel")
      else
        flash[:notice] = "No hay candidatos en esta tabla"
        redirect_to :back 
      end
    else
      if exist_profiles
        send_file(@file, filename: ""+@job.title+"_aceptados.xlsx", type: "application/vnd.ms-excel")
      else
        flash[:notice] = "No hay candidatos en esta tabla"
        redirect_to :back 
      end
    end
  end

  def get_job_resumes
    @job = Job.find(params[:job_id])
    @file = "#{Rails.root}/files/cvs.zip"
    @file = "#{Rails.root}/public/cvs.zip" if params[:application_ids].present?
    @applications = Application.get_applicants_all(@job.id)
    @applications = Application.where(id: params[:application_ids]) if params[:application_ids].present?
    Zip::ZipOutputStream.open(@file) do |zos|
      @applications.each do |application|
        user = application.user
        if user.account.resume.present?
          zos.put_next_entry(user.account.resume.filename)
          zos.print(URI.parse(user.account.resume.url).read) if user.account.resume_ok?
        end
      end
    end
    return render text: '<meta http-equiv="refresh" content="0; url=' + "/cvs.zip" +  '" />' if params[:application_ids].present?
    send_file(@file, filename: ""+@job.slug+"_cvs.zip", type: "application/octet-stream")
  end

  def check_resume
    user =  User.find(params[:user_id])
    resp = user.account.resume_ok?
    render json: resp.to_s, status: 200
  end

  def download_resume
    df = DownloadFile.where(token: params[:token_file])

    if df.exists? and df.last.expire_at >= Time.now
      df = df.last
      url = df.user.account.resume.url
    else
      url ="http://firstjob.me/"
    end

    render text: '<meta http-equiv="refresh" content="0; url=' + url +  '" />'
  end  

  private
  def set_application
    @application = Application.find(params[:id])
  end

  def create_excel_file
    question_title = []
    @questions.each do |q|
      question_title.push("Pregunta")
      question_title.push("Respuesta")
    end
    columns = ["Fecha", "Nombre", "Apellido", "Email" , "Universidad", "Carrera", "Nivel de Ingles", "Renta", "Nivel Excel", "Curriculum", "Estado", "Experiencia"]
    col_quest = columns+question_title
    excel_file = Axlsx::Package.new
    excel_file.workbook.add_worksheet(name: "Postulantes") do |sheet|
      title = sheet.styles.add_style(:bg_color => "FF715C",:fg_color=>"FFF",:b => true, :border=>Axlsx::STYLE_THIN_BORDER,:alignment=>{:horizontal => :center})
      rest = sheet.styles.add_style(:border=>Axlsx::STYLE_THIN_BORDER) 
      sheet.add_row([(0..col_quest.length).map { |i| col_quest[i] }].flatten , :style=>title)

      @applications.each do |application|
        user = application.user
        df = DownloadFile.where(user_id: application.user_id, file_type: "Resume")

        if df.exists?
          df = df.last
        else
          df = DownloadFile.new
          df.user_id = application.user_id
          df.token = SecureRandom.urlsafe_base64(8)
          df.file_type = "Resume"
        end

        df.expire_at = Time.now + 1.months
        df.save

        answers1 = create_answers(application)

        if user.account.excel.present?
          row1 = create_row(application, user)
          last_row = row1 + answers1
          row = sheet.add_row last_row, :style => rest
        else
          row1 = create_row1(application, user)
          last_row = row1 + answers1
          row = sheet.add_row last_row, :style => rest
        end

        if application.user.account.resume.present?
          sheet.add_hyperlink :location => request.base_url + download_resume_path + "?token_file=" + df.token, :ref => "J#{row.index + 1}"
          sheet["J#{row.index + 1}"].color = '0000FF'
        end
      end
    end
    excel_file
  end

  def create_row(application, user)
    [
      application.created_at.strftime("%d-%b-%Y"), user.first_name, user.last_name,
      user.email, user.account.try(:university_name), user.account.principal_career_id, user.account.try(:english_level_name).try(:titleize),
      application.salary_expected, Account::EXCEL_LEVEL[user.account.excel.level][0], user.account.resume_column_to_s,
      user.account.try(:career_status_name).try(:titleize), user.account.try(:years_experience_name).try(:titleize)
    ]
  end

  def create_row1(application, user)
    [ application.created_at.strftime("%d-%b-%Y"), user.first_name, user.last_name, user.email, user.account.try(:university_name),
      user.account.principal_career_id, user.account.try(:english_level_name).try(:titleize), application.salary_expected, "No especifica",
      user.account.resume_column_to_s,
      user.account.try(:career_status_name).try(:titleize),
      user.account.years_experience_to_s
    ]
  end

  def create_answers(application)
    answers = application.answers.includes(:question)
    answers1 = []
    answers.each do |a|
      answers1.push(a.question.content)
      answers1.push(a.content)
    end
    answers1
  end

  def auth_user
    # TO DO: permit access to client, kam and admin only
  end

end