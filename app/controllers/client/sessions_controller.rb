class Client::SessionsController < Devise::SessionsController

  def create
    self.resource = warden.authenticate!(:scope => resource_name, :recall => "#{controller_path}#failure_company")
    sign_in(resource_name, resource)  
    render js: "window.location='#{client_process_index_url}';"
  end

  def failure_company
    render js: "$('.login-box').css('display', 'block')", status: 406
  end

end