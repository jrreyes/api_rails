class Client::PublishController < ApplicationController

  before_action :set_dependences, only: [:edit_step_1, :process_step_1]
  before_action :set_job, only: [:edit_step_1, :update, :destroy, :process_step_2, :process_step_3, :process_step_4]
  before_action :set_job, only: [:process_step_1_update, :process_step_2_update, :process_step_3_update, :process_step_4_update, :process_step_5_update]

  def process_step_1
    @url = client_create_process_path
    @job = Job.new
  end

  def edit_step_1
    @url = client_process_step_1_update_path
    @job = Job.find(params[:job_id])
    render template: "client/publish/process_step_1"
  end

  def create_process
    @job = Job.new(step_1_params)
    @job.state = 1
    @job.posted_by_id = current_company_user.id
    @job.published = false
    return render js: "window_location('#{client_process_step_2_path(@job.id)}')", status: 200 if @job.save
    render js: 'swal("Debes completar todos los campos.");'
  end

  def process_step_2
    @job = Job.find(params[:job_id])
    @universities = University.where(country_id: @job.countries).order(name: :asc)
    @careers = Career.where(country: @job.countries).order(name: :asc)
    @job_statuscareer = @job.job_statuscareer
  end

  def process_step_3
    @job = Job.find(params[:job_id])
    @values = [0,1,2,3,4,5]
  end

  def process_step_4
    @job = Job.find(params[:job_id])
    @questions = @job.questions.order(id: :asc)
  end

  def add_question
    job_id = params[:question][:job_id]
    content = params[:question][:content]
    question = Question.new(job_id: job_id, content: content)
    render js: "location.reload();", status: 200 if question.save
  end

  def delete_question
    question_id = params[:question_id]
    question = Question.find(question_id)
    render js: "location.reload();", status: 200 if question.destroy
  end

  def update_question
    question_id = params[:question_id]
    content = params[:content]
    question = Question.find(question_id)
    question.content = content
    question.save if question.job.company_id == current_company_user.company_id
    render js: "location.reload();", status: 200
  end

  def process_step_5
    @job = Job.find(params[:job_id])
  end

  def process_step_6
    @job = Job.find(params[:job_id])
  end

  def process_step_1_update
    is_ok = @job.update(step_1_params)
    @job.update(state: 1) if @job.state == nil
    return render js: "window_location('#{client_process_step_2_path(@job.id)}')", status: 200 if is_ok
    render js: 'swal("Debes completar todos los campos.");'
  end

  def process_step_2_update
    return render js: 'swal("Debes completar todos los campos.");' if step_2_params[:english_level].blank? || step_2_params[:excel_level].blank?
    @job.update(step_2_params)

    if params[:job].present?
      if params[:job][:job_statuscareer_attributes].present? && 
        (params[:job][:job_statuscareer_attributes][:student] == "0" or !params[:job][:job_statuscareer_attributes][:student].present?)
        @job.update(age_study: nil) 
      end
      if params[:job][:job_statuscareer_attributes].present? && 
        (params[:job][:job_statuscareer_attributes][:graduate] == "0" and params[:job][:job_statuscareer_attributes][:qualified] == "0")
        @job.update(years_experience: nil)
      end
    end 
    # if !params[:job][:job_statuscareer_attributes][:qualified].present?
    #   @job.update(years_experience: nil)
    # end
    render js: "window_location('#{client_process_step_3_path(@job.id)}')", status: 200
  end

  def process_step_3_update
    return render js: 'swal("Debes completar todos los campos.");', status: 500 if params[:job].blank? || ( params[:job].present? && params[:job].length < 5 )
    is_ok = @job.update(step_3_params)
    return render js: "window_location('#{client_process_step_4_path(@job.id)}')", status: 200 if is_ok
    render js: 'swal("Debes completar todos los campos.");', status: 500
  end

  def process_step_4_update
    @job.update(step_4_params) if params[:job].present?
    render js: "window_location('#{client_process_step_5_path(@job.id)}')", status: 200
  end

  def process_step_5_update
    @job.update(state: 2)
    Job.calcule_matchs(12, @job.id, nil)
    render js: "window_location('#{client_process_step_6_path(@job.id)}')", status: 200
  end

  def add_new_question
    @question_key =  SecureRandom.random_number(36**6).to_s(36).rjust(6, "0") # random 6 chars
    @count = params[:count]
    html = render_to_string "client/publish/_new_question", :layout => false
    render text: html, status: 200
  end

  private
  def set_dependences
    @countries = Country.all
  end

  def set_job
    @job = Job.find(params[:job_id])
  end

  def step_1_params
    params.require(:job).permit(:title, :description, :location, :show_salary, :looking_for, :company_id, :availability, :vacancies, :maximum_salary_offered, country_ids: [])
  end

  def step_2_params
    params.require(:job).permit(:looking_for, :years_experience, :english_level, :excel_level, :show_salary, :age_study, university_ids: [],
     career_ids: [], job_statuscareer_attributes: [:student, :graduate, :qualified, :id])
  end

  def step_3_params
    params.require(:job).permit(:filter_califications, :filter_assistantship, :filter_extra_activities, :filter_student_activities, :filter_student_project)
  end

  def step_4_params
    params.require(:job).permit!
  end

end