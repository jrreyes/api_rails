class Client::SearchController < ApplicationController

  require 'action_view/helpers/javascript_helper'
  include ActionView::Helpers::JavaScriptHelper

  def index
    @accounts = Account.search_accounts(current_company_user, params)
    @countries = Country.all
    @universities = University.for_countries(params[:countries_ids])
    @careers = Career.for_countries(params[:countries_ids])
  end

  def data_for_countries
    countries_ids = params[:countries_ids]
    @universities = University.for_countries(countries_ids)
    @careers = Career.for_countries(countries_ids)
    html_text = escape_javascript render_to_string "client/search/_universities_careers", :layout => false
    return render js: "$('#universities_careers').html('#{html_text}');" , status: 200
  end

end