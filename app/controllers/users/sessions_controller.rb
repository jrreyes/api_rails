class Users::SessionsController < Devise::SessionsController

  protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/vnd.myapp.v1' }

  def create

    respond_to do |format|
      format.html do
        super
      end

      format.json do

        # Add authentification token to User model
        # Cuando el usuario inicia sesión, se devuelve su token de acceso.
        # Éste será solicitado en cada petición a la API

        self.resource = warden.authenticate!(auth_options)
        sign_in(resource_name, resource)

        if user_signed_in?
          data = self.resource
          data.public_token = params[:user][:public_token] if params[:user][:public_token].present?
          data.save
          render json: data.user_data, status: 201
        else
          render text: "Invalid user or password!!", status: 401
        end
      end

    end
  end

end