class Users::FacebookController < ApplicationController
	#protect_from_forgery with: :null_session, only: :facebook
	def facebook
		oauth_access_token = params[:code]
		public_token = params[:public_token]
		@user = User.from_access_token_facebook(oauth_access_token, public_token)
		return redirect_to root_path if @user.nil?
		respond_to do |format|
			format.html { sign_in_and_redirect @user}
			format.json {render json: @user.user_data}
		end
	end
end