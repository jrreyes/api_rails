# encoding: UTF-8
class Users::UserController < ApplicationController
  #before_action :authenticate!, only: [:show, :download_resume, :resume_display]
  before_action :check_owner, only: [:show, :download_resume, :resume_display]
  before_action :find_user, only: [:show, :download_resume, :resume_display]
  before_action :auth_user!, except: [:show, :download_resume, :resume_display]

  protect_from_forgery with: :null_session, only: [:new_user_data]

  def me
    render json: current_user.user_data
  end

  def new_user_data
    user = User.find(current_user.id)
    user.set_user_data(new_data_user_params)
    return render json: user.user_data, status: 200 if user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500   
  end

  def user_data_academic
    user = User.find(current_user.id)
    user.set_data_academic(user_data_academic_params)
    return render json: user.user_data, status: 200 if user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500
  end

  def user_career_status
    user = User.find(current_user.id)
    user.set_career_status(user_career_status_params)
    return render json: user.user_data, status: 200 if user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500
  end

  def user_ability
    user = User.find(current_user.id)
    user.set_ability(user_ability_params)
    Job.calcule_matchs(12, nil, user.id)
    return render json: user.user_data, status: 200 if user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500
  end

  def user_salary
    user = User.find(current_user.id)
    user.set_salary(user_salary_params)
    return render json: user.user_data, status: 200 if user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500
  end

  def user_assistantships
    user = User.find(current_user.id)
    user.set_assistantships(params)
    return render json: user.user_data, status: 200 if user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500
  end

  def user_extra_activities
    activities_params = params[:user]
    current_user.set_extra_activities(activities_params)
    return render json: current_user.user_data, status: 200 if current_user.save
    return render json: {message: "error: " + user.errors.full_messages.to_s}, status: 500
  end

  def show
    @user = User.select("id, email, first_name, last_name").where(id: @user.id)
    render json: @user
  end

  def get_removed
    render json: current_user.removed_jobs_per_page(params[:page]), status: 200
  end

  def get_favorites
    render json: current_user.favorites_jobs(params[:page]), status: 200
  end

  def counter_jobs_user
    render json: current_user.counter_jobs, status: 200
  end

  def get_applications
    render json: current_user.applications_jobs(params[:page]), status: 200
  end

  def get_job_answers
    job = find_job_by_params(params[:job_id])
    render json: current_user.get_job_answers(job.id), status: 200
  end

  def get_job_application
    job = find_job_by_params(params[:job_id])
    render json: Application.user_apply?(current_user.id, job.id), status: 200
  end

  def job_remove
    job = find_job_by_params(params[:job_id])
    source = params[:source]
    data = {job_id: job.id, user_id: current_user.id}
    removed_job = RemovedJob.where(data)
    if removed_job.exists?
      rjob = removed_job.last
      rjob.removed = rjob.removed.present? ? false : true
    else
      rjob = RemovedJob.new(data)
      rjob.removed = false
    end
    ButtonTracking.track_event(current_user.id, job.id, rjob.removed, "remove", source)
    return render json: {message: "ok", removed: rjob.removed}, status: 200 if rjob.save
    render json: {message: "error" + rjob.errors.full_messages.to_s}, status: 500
  end

  def job_favorite
    job = find_job_by_params(params[:job_id])
    source = params[:source]
    data = {job_id: job.id, user_id: current_user.id}
    favorite = Favorite.where(data)
    if favorite.exists? 
      fav = favorite.last
      fav.removed = fav.removed.present? ? false : true
    else
      fav = Favorite.new(data)
      fav.removed = false
    end
    ButtonTracking.track_event(current_user.id, job.id, fav.removed, "favorite", source)
    return render json: {message: "ok", favorite: fav.removed}, status: 200 if fav.save
    render json: {message: "error"}, status: 500
  end

  def set_user_answers
    answers = params[:answers]
    persisted = current_user.set_answers(answers)
    return render json: {message: "ok"}, status: 200 if persisted
    return render json: {message: "error"}, status: 500
  end

  def resume_display    
  end

  def upload_resume
    filename = params[:file]
    current_user.account.resume = filename
    current_user.save!
    render json: current_user.user_data, status: 200
  end

  def download_resume
    if @user.has_resume?
      filename = @user.account.resume.path
      s3 = Aws::S3::Client.new(region:'sa-east-1')
      aws_object = s3.get_object(bucket:'firstjobcvs', key: filename).body.read
      send_data(aws_object, :filename => filename, :disposition => 'attachment')
    end
  end

  # == Private methods
  private
  def check_owner
    if current_user && (current_user.slug != params[:id])
      redirect_to root_path, flash: { error: 'You are not allowed to perform this action.' }
    end
  end

  def find_user
    @user = User.find(params[:id])
  end

  def new_data_user_params
    params.require(:users).permit(:first_name, :last_name, :country_id, :phone, :rut_dni) # etc
  end

  def user_data_academic_params
    params.require(:users).permit(:university_id, :principal_career_id)
  end

  def user_career_status_params
    params.require(:users).permit(:looking_for, :career_status, :years_experience, :age_study)
  end

  def user_ability_params
    params.require(:users).permit(:excel_level, :english_level)
  end

  def user_salary_params
    params.require(:users).permit(:minimum_salary_expected)
  end
end