class Users::JobsController < ApplicationController

  before_action :set_job, only: [:show, :edit, :update, :destroy, :get_job_questions]
  before_action :auth_user!, only: [:answers_job_questions]
  protect_from_forgery with: :null_session, only: [:job_apply]

  def index
    respond_to do |format|
      format.html {
        @jobs = Job.all_jobs(current_user)
      }
      format.json {
        render :json => Job.get_all_jobs(current_user, params[:page])
      }
    end
  end

  def show
    respond_to do |format|
      format.html
      format.json {
        render :json => @job.get_details(current_user)
      }
    end
  end

  def filter_jobs
    key_value = params[:key_value]
    jobs = Job.get_all_jobs_no_page(current_user)
    result = Job.filter_jobs(jobs, key_value)
    render json: Job.get_data_jobs(result), status: 200
  end

  def get_job_questions
    render json: @job.questions.order(id: :asc), status: 200
  end

  def answers_job_questions
    answers = params[:answers]
    job = find_job_by_params(params[:job_id])
    return render json: {message: "error"} unless Job.where(id: job.id).exists?
    app = Application.user_apply?(current_user.id, job.id)
    answers.each do |ans|
      data = {application_id: app.id, question_id: ans[:question_id]}
      answer = Answer.where(data).exists? ? Answer.where(data).last : Answer.new(data)
      answer.content = ans[:value]
      return render json: {message: "error: " + answer.errors.full_messages.to_s} unless answer.save
    end
    render json: {message: "ok"}, status: 200
  end

  def job_apply
    job = find_job_by_params(apply_job_params[:job_id])
    application = Application.user_apply?(current_user.id, job.id)
    salary = apply_job_params[:minimum_salary_expected].to_i
    app = application.present? ? application : Application.new(user_id: current_user.id, job_id: job.id)
    app.salary_expected = salary
    app.user_removed = nil
    return render json: {message: "ok"}, status: 200  if app.save
    render json: {message: "error " + app.errors.full_messages.to_s}, status: 500
  end

  def job_apply_undo    
    job = find_job_by_params(params[:job_id])
    app = Application.user_apply?(current_user.id, job.id)
    return render json: {message: "error"}, status: 500 if app.nil?
    app.user_removed = true
    return render json: {message: "removed"}, status: 200 if app.save
    return render json: {message: "error"}, status: 500
  end

  def button_click_tracking
    job = find_job_by_params(params[:job_id])
    source = params[:source]
    source_utm = params[:source_utm]
    button_type = params[:button_type]
    public_token = params[:public_token]
    if current_user.present?
      response = nil
      response = ShowMoreTracking.track_event(current_user.id, job.id, source, source_utm) if button_type == "show-more"
      response = GetMoreTracking.track_event(current_user.id, job.id, source, source_utm) if button_type == "get-more"
      return render json: {message: response}, status: 200
    else
      response = ShowMoreTrackingAnon.track_event(public_token, job.id, source, source_utm) if button_type == "show-more"
      response = GetMoreTrackingAnon.track_event(public_token, job.id, source, source_utm) if button_type == "get-more"
      return render json: {message: response}, status: 200
    end
    render json: {message: "error"}, status: 500
  end

  def share_click_tracking
    job = find_job_by_params(params[:job_id])
    network = params[:network]
    source_utm = params[:source_utm]
    if current_user.present?
      response = nil
      response = ShareTracking.track_event(current_user.id, job.id, network, source_utm)
      return render json: {message: response}, status: 200
    end
    render json: {message: "error"}, status: 500
  end

  def data_for_filters
    render json: Job.data_for_filters(current_user), status: 200
  end

  def get_filtered_jobs
    careers_ids = params[:job][:ids]
    experience = params[:job][:experience]
    looking_for = params[:job][:looking_for]
    career_status = params[:job][:career_status]
    jobs_ids = Career.getJobsFromCareers(current_user, careers_ids)

    p "-----------------"
    p career_status
    p career_status == 1

    jobs = Job.all_jobs(current_user) if jobs_ids.blank?
    jobs = jobs.where("years_experience <= ?", experience) if experience.present?
    jobs = jobs.where("looking_for = ?", looking_for) if looking_for.present? and jobs.present?
    #@accounts = @accounts.joins(:newdatauser).where(newdatausers: {age_study: params[:age_study]}) if params[:age_study].present?
    jobs = jobs.joins(:job_statuscareer).where("job_statuscareers.student = ?", true) if career_status == "1"
    jobs = jobs.joins(:job_statuscareer).where("job_statuscareers.qualified = ?", true) if career_status == "2"
    jobs = jobs.joins(:job_statuscareer).where("job_statuscareers.graduate = ?", true) if career_status == "3"
    res = Job.get_jobs_data(current_user, jobs, 1)
    render json: res, status: 200
  end

  private
  def set_job
    @job = Job.find(params[:id])
  end

  def job_params
    params.require(:job).permit(:title, :show_salary, :location, :company_id) # etc
  end

  def apply_job_params
    params.require(:application).permit(:job_id, :minimum_salary_expected)    
  end
end
