class Users::PasswordsController < Devise::PasswordsController

  prepend_before_filter :require_no_authentication
  protect_from_forgery with: :null_session, only: [:create], :if => Proc.new { |c| c.request.format == 'application/vnd.myapp.v1' }

  # POST /resource/password
  def create

    self.resource = resource_class.send_reset_password_instructions(resource_params)
    yield resource if block_given?

    if successfully_sent?(resource)
      render json: {message: "ok"}, status: 200
    else
      render json: {message: "error: " + resource.errors.full_messages.to_s}, status: 500
    end
  end

end