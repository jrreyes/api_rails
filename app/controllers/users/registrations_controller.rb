class Users::RegistrationsController < Devise::RegistrationsController

  prepend_before_action :require_no_authentication, only: [:new, :create, :cancel]
  prepend_before_action :authenticate_scope!, only: [:edit, :update, :destroy]

  protect_from_forgery with: :null_session, only: [:create], :if => Proc.new { |c| c.request.format == 'application/vnd.myapp.v1' }

  # POST /resource
  def create

    build_resource() # create a new empty user
    return render json: {message: "La contraseña debe ser de 8 caractares como mínimo."}, status: 422 if params[:password].length < 8
    resource.email = params[:email] if params[:email].present?
    resource.password = params[:password] if params[:password].present?
    resource.first_name = params[:first_name] if params[:first_name].present?
    resource.public_token = params[:public_token] if params[:public_token].present?
    resource.account = Account.new
    resource.save
    yield resource if block_given?
    if resource.persisted?
      UserMailer.welcome(resource).deliver_now
      render json: resource.user_data, status: 200
    else
      render json: {res: 'Ya existe un usuario con ese email. Reintenta nuevamente.'} , status: 409
    end
  end

end