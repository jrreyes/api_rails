#encoding: utf-8
class Notifier < ActionMailer::Base
  default from: 'FirstJob.me <noreply@firstjob.cl>'
  default to: 'contacto@firstjob.cl'

  def new_message(message)
    @message = message
    mail(subject: 'Contacto desde FirstJob')
  end

  def new_post_request(post_request)
    @post_request = post_request
    mail(bcc: 'mario@firstjob.cl' , subject: '[Compañia-Post-Falso] Aviso')
  end
end
