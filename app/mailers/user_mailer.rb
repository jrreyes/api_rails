class UserMailer < ActionMailer::Base
  default from: 'contacto@firstjob.cl'

  def new_referred(referred)
    @referred = referred
    mail(to: @referred.email, subject: 'Hay una nueva oferta de trabajo esperando por ti')
  end

  def welcome(user)
    @user = user
    mail(to: @user.email, subject: "Bienvenido a FirstJob.me! ")
  end

end