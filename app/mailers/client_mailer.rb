class ClientMailer < ActionMailer::Base

  def request_new_candidate(company_user, job, comments)
    email_to = "kam@firstjob.me"
    email_to = "abel@firstjob.cl" if Rails.env.development?
    @company_user = company_user
    @comments = comments
    @job = job
    mail(from: "FirstJob.me <#{@company_user.email}>", to: email_to, subject: 'Nueva solicitud de candidato')
  end

  def add_new_candidate(first_name, last_name, email, password)
    @origin = "contacto@firstjob.me"
    @email_to = email
    @password = password
    @first_name = first_name
    @last_name = last_name
    @email_to = "abel@firstjob.cl" if Rails.env.development?
    mail(from: "FirstJob.me <#{@origin}>", to: @email_to, subject: 'Hay un trabajo esperando por ti')
  end

end