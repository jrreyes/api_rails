$(document).on("ready", function(){
  console.log("jquery ready 2! companies");

(function($) {
  /* Sticky navigation */
  var stickyNav = function() {
    var header = $('[data-js="header"]');
    var headerFixed = $('[data-js="header-fixed"]');
    var headerHeight = header.outerHeight();
    var scrollTop = $(window).scrollTop();

    if (header.length && scrollTop >= headerHeight) {
      $('.navigation').addClass('is-sticky');
    } else {
      $('.navigation').removeClass('is-sticky');
    }
    if (headerFixed.length) {
      $('.navigation').addClass('is-sticky');
    }
  }
  stickyNav();
  $(window).scroll(function() {
    stickyNav();
  });
  /* Toggle mobile menu */
  $('.main-nav-toggle').click(function(e) {
    e.preventDefault();
    var obj = $('.main-nav');
    var sel = 'is-open';

    if ($(obj).hasClass(sel)) {
      $(obj).removeClass(sel);
      $('body').removeClass('is-behind');
    } else {
      $(obj).addClass(sel);
      $('body').addClass('is-behind');
    }
  })
  /* Smooth scrolling to anchor link */
  $('[data-scroll="smooth"]').click(function(e) {
    e.preventDefault();
    var hash = this.hash,
    section = $(hash);

    if (hash.length) { 
      $('html, body').animate({
        scrollTop: section.offset().top
      }, 1000, 'swing', function(){
        history.replaceState({}, '', hash);
      });
    }
  });
  /* Prevent the login box closes */
  $('.dropdown-menu').click(function(e) {
    e.stopPropagation();
  })
  /* Form validation */
  $(document).on('click', '.disable-after-click' , function(){

    button = $(this);
    valid = false;
    valid_publish = false;

    if ($('#new_message').length) {
      valid = $('#new_message').valid();
    }
    if ($('#publish_form').length) {
      valid_publish = $('#publish_form').valid(); 
    }
    if ( valid || valid_publish ){
      setTimeout(function(){
        button.attr('disabled', true);
      }, 30);
    }
  });
  /* Set current menu */
  $(document).on("ready", function(){
    var currentMenu = $('[data-current-menu]').data('current-menu');
    if (currentMenu) {
      $('#'+currentMenu).addClass('is-active');
    }
    /* Remove active class when hover on a link, and renew active class on mouse leave */
    $('.main-nav-link').hover(function() {
      if ($('.main-nav-link').hasClass('is-active')) {
        $('.main-nav-link').removeClass('is-active');
      } else {
        $('#'+currentMenu).addClass('is-active');
      }
    });
  });

  /* Translated validation error messages to Spanish */
  $.extend($.validator.messages, {
    required: "Este campo es obligatorio.",
    remote: "Por favor, rellena este campo.",
    email: "Por favor, escribe una dirección de correo válida.",
    url: "Por favor, escribe una URL válida.",
    date: "Por favor, escribe una fecha válida.",
    dateISO: "Por favor, escribe una fecha (ISO) válida.",
    number: "Por favor, escribe un número válido.",
    digits: "Por favor, escribe solo dígitos.",
    creditcard: "Por favor, escribe un número de tarjeta válido.",
    equalTo: "Por favor, escribe el mismo valor de nuevo.",
    extension: "Por favor, escribe un valor con una extensión aceptada.",
    maxlength: $.validator.format("Por favor, no escribas más de {0} caracteres."),
    minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
    rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
    range: $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
    max: $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
    min: $.validator.format("Por favor, escribe un valor mayor o igual a {0}."),
    nifES: "Por favor, escribe un NIF válido.",
    nieES: "Por favor, escribe un NIE válido.",
    cifES: "Por favor, escribe un CIF válido."
  });
  /* Console Message */
  var FirstJob = [
  'padding: 5px 10px',
  'border-radius: 50px',
  'background: #ff715b',
  'color: #fff',
  'font-family: Arial, sans-serif',
  'font-size: 14px',
  'font-weight: bold',
  'line-height: 2',
  'text-shadow: 0 1px 0 rgba(0, 0, 0, 0.3)'
  ].join(';');

  console.log('%cFirstJob.me', FirstJob);

})(jQuery);

var contact_ok = function(){

  setTimeout(function(){ 
    $('#contact_ok_message').text('¡Tu mensaje se ha enviado con éxito!');
    $('#new_message').css('display', 'none');
    $('#contact_ok_message').css('display', 'block');
  }, 1000);

  setTimeout(function(){ 
    // location.reload();
  }, 5000);

}

var contact_error = function(){
  setTimeout(function(){ 
    $('#contact_ok_message').text('Ha ocurrido un error. Reintenta nuevamente.');
    $('#contact_ok_message').css('display', 'block');
  }, 1000);
}

var publish_ok = function(){

  setTimeout(function(){ 
    $('#publish_form').css('display', 'none');
    $('#publish_ok_message').css('display', 'block');
  }, 500);
  
  setTimeout(function(){ 
    location.reload();
  }, 5000);

}
});
