// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery2
//= require jquery_ujs
// require turbolinks
//= require bootstrap-sprockets
//= require pace/pace
//= require select2
//= require sweetalert2.min.js

$(document).on("ready", function(){
    console.log("jquery ready! app");
    $(".display-resume").on("click", function(){
        url = $(this).data("url");
        $.get( url, function( data ) {
          $(".display-resume-block").html(data);
          $(".display-resume-modal").modal("show");
        });
    });

    $(".download-resume").on("click", function(){
    url = $(this).data("url");
    window_location(url);
    });
    like_buttons();
    add_new_questions();
    change_job_state();
    delete_question();
    load_universities_and_careers();
    user_panel_click();
    download_user_data();
    download_user_resume();
    republish_job();
    edit_comment();
    delete_comments();
    edit_question();
    close_process();

    /*  Publicar proceso  */

    $(".job_looking_for span label").on("click", function(){
      $('.job_looking_for input:not(:checked)').parent().removeClass("checked");
      $('.job_looking_for input:checked').parent().addClass("checked");
    });

    $(".job_availability span label").on("click", function(){
      $('.job_availability input:not(:checked)').parent().removeClass("checked");
      $('.job_availability input:checked').parent().addClass("checked");
    });

    $(".job_years_experience span label").on("click", function(){
      $('.job_years_experience input:not(:checked)').parent().removeClass("checked");
      $('.job_years_experience input:checked').parent().addClass("checked");
    });

    $(".job_age_study span label").on("click", function(){
      $('.job_age_study input:not(:checked)').parent().removeClass("checked");
      $('.job_age_study input:checked').parent().addClass("checked");
    });

    $(".job_english_level span label").on("click", function(){
      $('.job_english_level input:not(:checked)').parent().removeClass("checked");
      $('.job_english_level input:checked').parent().addClass("checked");

    });

    $(".job_excel_level span label").on("click", function(){
      $('.job_excel_level input:not(:checked)').parent().removeClass("checked");
      $('.job_excel_level input:checked').parent().addClass("checked");
    });

    $(".job_career_status span input:checkbox").change(function(){
      if ($(this).is(':checked')) {
        $(this).parent().addClass("checked");
        if(this.id == "job_job_statuscareer_attributes_student" || this.id == "job_career_status_student"){
          $(".job_age_study").parent().removeClass("hidden");
        }
        if(this.id == "job_job_statuscareer_attributes_graduate" || this.id == "job_job_statuscareer_attributes_qualified"
          || this.id == "job_career_status_graduate" || this.id == "job_career_status_qualified" ){
          $(".job_years_experience").parent().removeClass("hidden");
        }
      }else{
          $(this).parent().removeClass("checked");
          if(this.id == "job_job_statuscareer_attributes_student" || this.id == "job_career_status_student"){
            $(".job_age_study").parent().addClass("hidden");
            $(".job_age_study span label input[type=radio]:checked").parent().removeClass("checked");
            $(".job_age_study span label input[type=radio]:checked").attr('checked', false);
          }
          if(this.id == "job_job_statuscareer_attributes_graduate" || this.id == "job_career_status_graduate"){
            if (!$("#job_job_statuscareer_attributes_qualified").is(':checked')) {
              if (!$("#job_career_status_qualified").is(':checked')) {
                $(".job_years_experience").parent().addClass("hidden");
                $(".job_years_experience span label input[type=radio]:checked").parent().removeClass("checked");
                $(".job_years_experience span label input[type=radio]:checked").attr('checked', false);
              }
            }
          }
          if(this.id == "job_job_statuscareer_attributes_qualified" || this.id == "job_career_status_qualified" ){
            if (!$("#job_job_statuscareer_attributes_graduate").is(':checked')){
              if (!$("#job_career_status_graduate").is(':checked')) {
                $(".job_years_experience").parent().addClass("hidden");
              }
            }
          }
        }
    });

    $('.process_publish input:radio').each(function() {
      if ($(this).is(':checked')) {
        $(this).parent().addClass('checked');
      }
    });

    $('.basics_filter input:radio').each(function() {
      if ($(this).is(':checked')) {
        $(this).parent().addClass('checked');
      }
    });

    $('.basics_filter input:checkbox').each(function() {
      if ($(this).is(':checked')) {
        $(this).parent().addClass('checked');
        if (this.id == "job_job_statuscareer_attributes_student") {
          $(".job_age_study").parent().removeClass("hidden");
        }
        if (this.id == "job_job_statuscareer_attributes_graduate") {
          $(".job_years_experience").parent().removeClass("hidden");
        }
        if (this.id == "job_job_statuscareer_attributes_qualified") {
          $(".job_years_experience").parent().removeClass("hidden");
        }
      }
    });

    /*  Fin Publicar Proceso  */
    /* Search Cliente */
    $(".choose_bd span label").on("click", function(){
        $('.choose_bd input:not(:checked)').parent().removeClass("checked");
        $('.choose_bd input:checked').parent().addClass("checked");
    });
    $('.search input:radio').each(function() {
        if ($(this).is(':checked')) {
            $(this).parent().addClass('checked');
        }
    });
    /* Fin Search Cliente */
});

function setLikesValue(application_id, value){

  if (value.toString() == ""){
    $(".unlike-btn-" + application_id).attr("data-current-value", 0);
    $(".like-btn-" + application_id).attr("data-current-value", 0);
  }

  if (value.toString() == "true"){
    $(".unlike-btn-" + application_id).attr("data-current-value", 0);
    $(".like-btn-" + application_id).attr("data-current-value", 1);
  }
  
  if (value.toString() == "false"){
    $(".unlike-btn-" + application_id).attr("data-current-value", 1);
    $(".like-btn-" + application_id).attr("data-current-value", 0);
  }

}

function like_buttons(){
  $(".like-buttons").on("click", function(){
    var button_type= $(this).attr("data-button-type");
    var application_id = $(this).attr("data-application-id");
    var current_value = $(this).attr("data-current-value");

    var url = $(this).data("url");
    $.post( url, {button_type: button_type, application_id: application_id, current_value: current_value}, function( data ) {

    });
  });
}

function window_location(url){
  window.location = url; 
}

function add_new_questions(){
  $(".add-new-question").on("click", function(){
    var url = $(this).attr("data-url");
    var question_count = $(".question-input").length;    
    $.get( url, {count: question_count}, function( data ) {
      $(".all_questions").append(data);
    });
  });
}

function share_on_facebook(){
  var url = "http://google.cl/";
  window.open('https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(url), 'facebook-share-dialog', 'width=626,height=436');
  return false;
}

function share_on_linkedin(){
  var url = "http://google.cl/";
  window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + url + '&source=FirstJob', 'facebook-share-dialog', 'width=626,height=436');
  return false;
}

$(document).on("ready", function(){
  if ($( "#job_country_ids").length > 0){
    $( "#job_country_ids").select2({
      theme: "bootstrap"
    });
  }
  if ($( "#job_university_ids").length > 0){
    $( "#job_university_ids").select2({
      theme: "bootstrap"
    });
  }
  if ($( "#job_career_ids").length > 0){
    $( "#job_career_ids").select2({
      theme: "bootstrap"
    });
  }

  if ($("[data-type=select2]").length > 0){
    $( "[data-type=select2]").select2({
      theme: "bootstrap"
    });
  };

});

/* NEW JOB*/
function delete_question(){
  $(document).on("click", ".remove-question", function(){
    question_id = $(this).attr("data-id");
    $(".question-" + question_id).css("display", "none");
    $(".destroy-" + question_id).val("1");
  });
}

function load_universities_and_careers(){
  $('#countries_ids').on("change", function(e) {
    var countries_ids = $('#countries_ids').val();
    console.log(countries_ids);
    if (!countries_ids){
      return true;
    }
    url_universities = "/client/data-for-countries";
    $.post( url_universities, {countries_ids: countries_ids}, function( data ) {
      $("#universities_careers").html(data.universities);
      select_2_loader();
    });
  });
}

function select_2_loader(){
  if ($("[data-type=select2]").length > 0){
    $("[data-type=select2]").select2({
      theme: "bootstrap"
    });
  };
}

function user_panel_click(){

  $(".tab-perfil, .btn-information").on("click", function(){
    var button = $(this);
    var app_id = button.data("app-id");
    var url = button.data("url");
    var user_id = button.data("user-id");
    $.get( url, {app_id: app_id}, function( data ) {
      $(".pane-perfil-" + user_id).html(data);
    });
  });

  $(".tab-procesos").on("click", function(){
    var button = $(this);
    var app_id = button.data("app-id");
    var url = button.data("url");
    var user_id = button.data("user-id");
    $.get( url, {app_id: app_id}, function( data ) {
      $(".pane-procesos-" + user_id).html(data);
    });
  });

  $(".tab-respuestas").on("click", function(){
    var button = $(this);
    var app_id = button.data("app-id");
    var url = button.data("url");
    var user_id = button.data("user-id");
    $.get( url, {app_id: app_id}, function( data ) {
      $(".pane-respuestas-" + user_id).html(data);
    });
  });

  $(".tab-comentarios").on("click", function(){
    var button = $(this);
    var app_id = button.data("app-id");
    var url = button.data("url");
    var user_id = button.data("user-id");
    $.get( url, {app_id: app_id}, function( data ) {
      $(".pane-comentarios-" + user_id).html(data);
    });
  });

}

function request_new_candidate(job_id){
  $("#request_new_candidate-button-" + job_id).text("Candidatos solicitados");
  $("#request_new_candidate-" + job_id).collapse('hide');
  $("#text_request_new_candidate-" + job_id).val('');
}

function edit_application_comment(user_id, comment_id, comment){
  console.log(user_id, comment_id, comment);
  $(".input-comment-id-" + user_id).val(comment_id);
  $(".text_application_comment-" + user_id).val(comment);
}


function download_user_data(){
  $(".download_user_data").on("click", function(){
    var url = $(this).data("url");
    //console.log(url);
    var data = $(".checkbox_user:checked");
    var application_ids = [];
    data.map(function() {
      application_ids.push(this.value);
      return true;
    });

    $.post(url, {application_ids: application_ids}, function(data){
      $("head").append(data);
    });

  });
}

function download_user_resume(){
  $(".download_user_cv").on("click", function(){
    var url = $(this).data("url");
    var data = $(".checkbox_user:checked");
    var application_ids = [];
    data.map(function() {
      application_ids.push(this.value);
      return true;
    });
    $.post(url, {application_ids: application_ids}, function(data){
      $("head").append(data);
    });

  });
}

function check_resume(user_id){
  $.get("/client/users/" + user_id + "/check_resume", {}, function(resume_ok){
    if (resume_ok.toString() == "false"){
      $(".cv-status-" + user_id).css("display", "inline-block");
    }
  });
}

/* SWEETALERT2 CUSTOMS */
function republish_job(){
    $(document).on("click", '.republish-job', function(){
        var url = $(this).data("url");
        swal({
            title: "¿Estás seguro/a?",
            text: "El nuevo proceso estará oculto hasta que lo publiques",
            type: "warning",
            showCancelButton: true,
            reverseButtons: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Duplicar proceso",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            html: false
        }).then(function(){
            swal({
                title: "Creado!",
                text: "El proceso ha sido creado exitosamente",
                type: "success",
                showConfirmButton: false,
                timer: 2000
            });
            setTimeout(function(){
                $.post( url, {}, function(res){
                    console.log(res);
                });
            }, 2000);
        }, function(err) {
            console.log(err); // Error: "It broke"
        });
    });
}

function edit_comment(){
    $(document).on("click", '.btn-comments-edit', function(){
        var comment_id = $(this).data("comment-id");
        var content = $(this).data("comment-content");

        swal({
            title: "Editar comentario",
            text: "Recuerda que si modificas un comentario se sobrescribe el anterior.",
            input: 'textarea',
            inputValue: content,
            confirmButtonColor: "#e1705b",
            reverseButtons: false,
            customClass: 'swal-edit-comment',
            cancelButtonText: "Cancelar",
            confirmButtonText: "Guardar",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            animation: "slide-from-top"
        }).then(function(inputValue){
            if (!inputValue){
                return;
            }
            $.post("/client/users/edit_comment", {comment_id: comment_id, content: inputValue}, function(res){
                console.log(res);
            });
        }, function(err) {
            console.log(err); // Error: "It broke"
        });
    });
}

function delete_comments(){
    $(document).on("click", '.comments_delete', function(){
        var url = $(this).data("url");
        swal({
            title: "¿Deseas Eliminar el comentario?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            reverseButtons: false,
            confirmButtonText: "Eliminar Comentario",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            html: false
        }).then(function(){
            swal({
                title: "Eliminado!",
                text: "El comentario se elimino satisfactoriamente!",
                type: "success",
                showConfirmButton: false,
                timer: 2000
            });
            setTimeout(function(){
                $.post( url, {}, function(){
                });
            }, 2000);
        }, function(err) {
            console.log(err); // Error: "It broke"
        });
    });
}


function delete_question(){
  $(".delete-question").on("click", function(){
    var question_id = $(this).data("question-id");
    var content = $(this).data("question-content");

    swal({
      title: "¿Deseas eliminar esta pregunta?",
      text: content,
      type: "warning", 
      customClass: 'swal-delete-question',
      showCancelButton: true,
      reverseButtons: false,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Eliminar",
      cancelButtonText: "Cancelar",
      showLoaderOnConfirm: true,
      closeOnConfirm: false,
      html: false
    }).then(function(){
        swal({
            title: "Eliminada!",
            text: "La pegunta se eliminó correctamente!",
            type: "success",
            showConfirmButton: false,
            timer: 2000
        });

        setTimeout(function(){
            $.post("/client/publish/delete-question", {question_id: question_id}, function(res){
                console.log(res);
            });
        }, 2000);

    }, function(err) {
        console.log(err); // Error: "It broke"
    });
  });
}

function edit_question(){
  $(".update-question").on("click", function(){
    var question_id = $(this).data("question-id");
    var content = $(this).data("question-content");

    swal({
      title: "Editar pregunta",
      text: 'Recuerda no cambiar el contexto de la pregunta si ya tiene respuestas.',
      input: 'textarea',
      inputValue: content,
      confirmButtonColor: "#FA8134",
      reverseButtons: false,
      customClass: 'swal-edit-question',
      cancelButtonText: "Cancelar",
      confirmButtonText: "Guardar",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
      animation: "slide-from-top"
    }).then(function(inputValue){
      if (!inputValue){
        return;
      }
      $.post("/client/publish/update-question", {question_id: question_id, content: inputValue}, function(res){
        console.log(res);
      });
    }, function(err) {
        console.log(err); // Error: "It broke"
    });
  });
}
function close_process(){
    $(".close-process").on("click", function(){
        var url = $(".update_job_state_url").data("url");
        var old_state =$(this).data("old-state");
        var state = $(this).data("state");
        var job_id = $(this).data("job-id");
        if (old_state != 4){
            swal({
                title: "¿Deseas cerrar este proceso?",
                text: 'Recuerda que si cierras el proceso no se puede volver abrir.',
                type: "warning",
                showCancelButton: true,
                reverseButtons: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Cerrar Proceso",
                customClass: 'swal-close-process',
                cancelButtonText: "Cancelar",
                showLoaderOnConfirm: true,
                closeOnConfirm: false,
                html: false
            }).then(function(){
                swal({
                    title: "Proceso Cerrado!",
                    type: "success",
                    showConfirmButton: false,
                    timer: 2000
                });

                setTimeout(function(){
                    $.post( url, {state: state, job_id: job_id}, function(res){
                        console.log(res);
                    });
                }, 2000);

            }, function(err) {
                console.log(err); // Error: "It broke"
            });
        }
    });
}

function change_job_state(){
    $(document).on("click", '.change-job-state', function(){
        var url = $(".update_job_state_url").data("url");
        var state = $(this).data("state");
        var job_id = $(this).data("job-id");
        $.post( url, {state: state, job_id: job_id}, function(){

        });
    });
}
/* FIN DE SWEETALERT2 CUSTOM */

$('.disabledjh').click(function(e){
  console.log("diabled");
 e.preventDefault();
})