class ShareTracking < ActiveRecord::Base
  belongs_to :user
  belongs_to :job

  validates :job_id, :user_id, :network, presence: true

  def self.track_event(user_id, job_id, network, source_utm)
    tracking = ShareTracking.new(user_id: user_id, job_id: job_id, network: network, source_utm: source_utm)
    tracking.save
  end

end
