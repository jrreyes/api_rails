class Company < ActiveRecord::Base

	# Low-Level Caché: http://guides.rubyonrails.org/caching_with_rails.html#low-level-caching

	# == Friendly url

	COMPANY_STATUS  = {
		1 => 'full',
		2 => 'Plan 1',
		3 => 'Trial',
		4 => 'Inactive'
	}

	extend FriendlyId
	friendly_id :name, use: [:slugged, :finders]
	
	has_many :company_users, dependent: :destroy

	has_many :jobs, dependent: :destroy # revisar relacion
  has_many :interviews, through: :jobs


	# images
	mount_uploader :avatar, AvatarUploader
	mount_uploader :square_image, SquareUploader
	mount_uploader :cover_image, CoverUploader
	mount_uploader :public_image, PublicImageUploader
	mount_uploader :slider_image, SliderImageUploader
	mount_uploader :img_share_fb, ImgShareFbUploader

	def get_company_users
		Rails.cache.fetch("company/#{self.id}/company_users", expires_in: 1.minute) do
			self.company_users
		end
	end

	def get_other_process user_id
			company_jobs = self.get_published_jobs
			Application.select("matchings.id as match_id, applications.job_id, applications.user_id").joins("LEFT OUTER JOIN matchings ON applications.job_id = matchings.job_id AND matchings.user_id = applications.user_id")
			.where("applications.user_id = ? AND applications.job_id IN (?) ", user_id, company_jobs.map(&:id))
	end

	def get_published_jobs
		Rails.cache.fetch("company/#{self.id}/published_jobs", expires_in: 1.minute) do
			self.get_jobs.where(published: true)
		end
	end

	def get_jobs		

		Rails.cache.fetch("company/#{self.id}/get_jobs", expires_in: 1.minute) do
			company_users = self.get_company_users
			jobs = Job.where(posted_by: company_users.map(&:id))
			return jobs
		end

	end

end