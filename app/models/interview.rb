class Interview < ActiveRecord::Base

  # == Associations
  belongs_to :company_user
  belongs_to :user
  belongs_to :job

  # == Validations
  validates :status, :job_id, :company_user_id, :user_id, :location, :starts_at, presence: true

  def self.get_interview_applicants job_id
    Interview.where(job_id: job_id)
  end
  
end