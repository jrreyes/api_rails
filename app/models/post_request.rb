class PostRequest < ActiveRecord::Base

  COMPANY_TYPE = {
    1 => 'Startup',
    2 => 'Pyme',
    3 => 'Gran empresa'
  }

  LOOKING_FOR   = {
    1 => 'Práctica',
    2 => 'Memoria',
    3 => 'Trabajo',
  }

  def self.company_type(id)
    return COMPANY_TYPE[id]
  end

  def self.job_type(id)
    return LOOKING_FOR[id]    
  end

end