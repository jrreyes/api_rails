class Application < ActiveRecord::Base

  # == Associations
  belongs_to :user
  belongs_to :job
  belongs_to :company_user

  has_many :answers, dependent: :destroy
  has_many :comments_applications, dependent: :destroy

  scope :removed, -> { where ("removed = true") }
  scope :by_user, ->(ids) { ids.any? ? where("applications.user_id IN (?)", ids) : where(created_at: nil) }
  scope :not_removed, ->  { where ("removed = false") }
  scope :not_accepted, ->(ids) { where("applications.user_id NOT IN (?)", ids) if ids.any? }

  scope :applied_jobs, -> { where("user_removed is NULL") }

  validates :job_id, :user_id, :salary_expected, presence: true

  def self.user_apply? user_id, job_id
    return nil if user_id.nil?
    job = Job.find(job_id)
    Application.where(user_id: user_id, job_id: job.id).where("user_removed is NULL").last
  end

  def self.get_applicants_all job_id
    Interview.get_interview_applicants job_id
    Application.where(job_id: job_id).order(created_at: :desc)
  end

  def get_answers
    answers = self.answers
    questions = []
    answers.each do |answer|
      questions.push({question: answer.question.content, answer: answer.content})
    end
    questions
  end

  def get_comments
    self.comments_applications
  end

  def is_match?
    Matching.is_match?(self.user_id, self.job_id)
  end

  def user_score
    # :filter_califications, :filter_assistantship, :filter_extra_activities, :filter_student_activities, :filter_student_project
    points = 10

    user = self.user
    job = self.job

    account = user.account if user.account.present?
    newdatauser = account.newdatauser if account.present?

    qualifications = newdatauser.qualifications.to_f * job.filter_califications.to_f if account.present? && newdatauser.present?
    assistantship = newdatauser.assistantship_number.to_f * job.filter_assistantship.to_f if account.present? && newdatauser.present?
    student_activities = points * job.filter_student_activities.to_f if account.student_activities.present?
    extra_activities = points * job.filter_extra_activities.to_f if account.extra_activities.present?
    student_projects = points * job.filter_student_project.to_f if account.student_projects.present?

    qualifications.to_f + assistantship.to_f + extra_activities.to_f + student_projects.to_f + student_activities.to_f
  end

  def self.add_applicant(current_company_user, email, first_name, last_name, job_id, send_email)
    user = User.where(email: email)
    pass = ''

    if user.exists?
      user = user.last
    else
      pass = Devise.friendly_token.first(8)
      user = User.new(email: email)
      user.password = pass
      user.password_confirmation = pass
      user.first_name = first_name
      user.last_name = last_name
      user.account = Account.new
      user.account.newdatauser = Newdatauser.new
      user.save
    end

    app = Application.where(user_id: user.id, job_id: job_id)
    
    if app.exists?
      return true
    else
      app = Application.new(user_id: user.id, job_id: job_id, salary_expected: 1, company_user_id: current_company_user.id)
      ClientMailer.add_new_candidate(first_name, last_name, email, pass) if send_email.present?
      return app.save
    end

    false
  end

end