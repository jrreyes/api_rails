class RemovedJob < ActiveRecord::Base
  belongs_to :user
  belongs_to :job

  def self.is_removed? user, job_id
    return false if user.nil?
    return RemovedJob.where(user_id: user.id, job_id: job_id, removed: false).exists?
  end

end
