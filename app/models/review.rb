#encoding: utf-8

class Review < ActiveRecord::Base
  # == Constants
  KIND = {
    1 => 'user',
    2 => 'company',
  }

  # == Carrierwave setup
  mount_uploader :avatar, AvatarUploader

  # == Validations
  validates :name, :profession, :avatar, :comment, :kind, presence: true

  # == Scope methods
  scope :user, -> { where(kind: 1) } 
  scope :company, -> { where(kind: 2) }

  # == Instance methods
  def full_description
    if kind == 1
      "#{name}, #{age} #{I18n.t(:years)} - #{profession}"
    else
      "#{name} - #{profession}"
    end
  end

  def kind_name
    KIND[kind]
  end
end
