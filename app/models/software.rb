class Software < ActiveRecord::Base
  
  # == Constants
  SOFTWARE_LEVEL = {
    1 => 'basic',
    2 => 'medium',
    3 => 'advanced'
  }
  
  # == Associations
  has_and_belongs_to_many :accounts

  # == Validations
  validates :level, :name, presence: true

end