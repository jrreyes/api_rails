class Excel < ActiveRecord::Base

	# == Constants
  SOFTWARE_LEVEL = [['No Maneja',0],['Basico',1],['Intermedio',2],['Profesional',3]]

  EXCEL_LEVEL = {
    0 => 'Not applicable',
    1 => 'Basic',
    2 => 'Medium',
    3 => 'Advanced'
  }

	# == Relations
	belongs_to :account

end