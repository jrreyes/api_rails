class CompanyUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable, :confirmable

  belongs_to :company
  has_many :posted_jobs, class_name: 'Job', foreign_key: 'posted_by_id', dependent: :destroy
  has_many :suggestions, class_name: 'Matching', foreign_key: 'suggested_by_id'
  has_many :interviews, dependent: :destroy
  has_many :video_interviews, dependent: :destroy
  has_many :applications, dependent: :destroy

  HIERARCHY  = {
  	1 => 'administrator',
  	2 => 'recruiter'
  }

  def self.is_kam?(company_user)
    true if company_user.company_id = 16
    false
  end

  def is_kam?
    self.current_company_id == 16
  end

end