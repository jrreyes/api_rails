class Job < ActiveRecord::Base

	# == Friendly url
	extend FriendlyId
	friendly_id :title, use: [:slugged, :finders]

	validates :title, :description, :looking_for, :location, presence: true

	WillPaginate.per_page = 10

	CAREER_STATUS = {
		1 => 'student',
		2 => 'graduate',
		3 => 'qualified'
	}

	SOFTWARE_LANGUAGE = [['No Maneja',0],['Basico',1],['Intermedio',2],['Profesional',3]]

	EXCEL_LEVEL = {
		0 => 'Not applicable',
		1 => 'Basic',
		2 => 'Medium',
		3 => 'Advanced'
	}

	ENGLISH_LEVEL = {
		1 => 'basic',
		2 => 'medium',
		3 => 'advanced',
		4 => 'native'
	}

	AVAILABILITY = {
		1 => 'Inmediata', 
		2 => 'Un mes',
		3 => 'Otra'
	}

	YEARS_EXPERIENCE = {
		1 => 'just_graduated',
		2 => 'one_year',
		3 => 'two_years_or_more'
	}

	belongs_to :currency
	belongs_to :company
	belongs_to :posted_by, class_name: 'CompanyUser'

	has_one :job_expansion, dependent: :destroy

	has_many :applications
	has_many :matchings
	has_many :interviews, dependent: :destroy
	has_many :video_interviews, dependent: :destroy
	has_many :questions, dependent: :destroy
	has_one :job_statuscareer, dependent: :destroy
	has_many :notifications, dependent: :destroy
	has_many :favorites, dependent: :destroy
	has_many :removed_jobs, dependent: :destroy

	has_and_belongs_to_many :universities#, after_add: :update_associations_flag, before_remove: :update_associations_flag

	has_and_belongs_to_many :countries, join_table: 'jobs_countries'
	has_and_belongs_to_many :careers

	accepts_nested_attributes_for :questions, :allow_destroy => true
	accepts_nested_attributes_for :job_statuscareer, allow_destroy: true

	# SCOPES
	scope :published, -> { where(published: true) }
	scope :unpublished, -> { where(published: false) }

	def basic_data user
		data = {
			id: self.id,
			title: self.title,
			avatar: self.avatar,
			description: self.description,
			location: self.location,
			slug: self.slug,
			looking_for: self.looking_for_name,
			created_at: self.created_at 
		}
		data[:company_logo] = self.get_company.present? ? self.get_company.avatar.url : ''
		data[:company] = self.get_company.name if self.get_company.present?
		data[:maximum_salary_offered] = self.show_salary ? self.maximum_salary_offered : 0
		data[:vacancies] = self.job_expansion.present? ? self.job_expansion.number_vacancies : 0
		data[:favorite] = Favorite.is_favorite?(user, self.id)
		data[:removed] = RemovedJob.is_removed?(user, self.id)
		data[:total_question] = Question.where(job_id: self.id).count
		data
	end

	def self.public_jobs
		Rails.cache.fetch("public-jobs", expires_in: 1.minute) do
			Job.where(published: true).where.not("(company_id = 2 OR company_id = 4 ) AND looking_for IN (1,2)").order(created_at: :desc)
		end
	end

	def self.get_all_jobs_from_company company_user, looking_for_name, order_by
		values = ["todos", "practica", "memoria", "trabajos"]
		company_id = company_user.current_company_id.present? ? company_user.current_company_id : company_user.company_id
		looking_for = [nil,1,2,3]
		looking_for = 1 if looking_for_name == values[1]
		looking_for = 2 if looking_for_name == values[2]
		looking_for = 3 if looking_for_name == values[3]
		jobs = Job.where(posted_by_id: company_user.id, looking_for: looking_for) if company_user.hierarchy.nil?
		jobs = Job.where(company_id: company_id, looking_for: looking_for) if company_user.hierarchy.present? || company_user.is_kam?
		jobs = jobs.order(created_at: :desc) if order_by == "created_at"
		jobs = jobs.order(title: :asc) if order_by == "title"
		jobs = jobs.order(state: :asc) if order_by == "state"
		return jobs
	end

	def self.get_job_details job_id
		data = {
			total_applicants: Application.get_applicants_in_job(job_id).count,
			total_match: Matching.get_matchings_for_job(job_id).count,
			info: Job.find(job_id).basic_data(nil)
		}
	end

	def set_state state_id
		self.state = state_id
		self.save
	end

	def get_matchings
		Matching.get_matchings_for_job(self.id)
	end

	# To Do: hacer una task para dejar todos los jobs iguales.
	# Algunos usan la tabla job y otros la tabla job_statuscareer.
	def get_career_status
		jcs = self.job_statuscareer
		return self.career_status if jcs.nil? && self.career_status.present?
		return [1,2,3] if jcs.nil? && self.career_status.nil?
		career_status_ids = []
		career_status_ids.push(1) if jcs.student.present?
		career_status_ids.push(2) if jcs.graduate.present?
		career_status_ids.push(3) if jcs.qualified.present?
		career_status_ids = [1,2,3] if career_status_ids.blank?
		career_status_ids
	end

	def self.all_jobs user
		jobs = user.present? ? user.all_jobs_user : Job.public_jobs
		jobs
	end

	def get_new_applicants current_company_user
		last_view = LastView.where(job_id: self.id, company_user_id: current_company_user)
		if last_view.exists?
			last_view = last_view.last
			newer_than = last_view.updated_at
			return Application.select(:user_id).where(job_id: self.id, removed: false).where("created_at >= ?", newer_than).map(&:user_id)
		end
		self.get_applicants_ids
	end

	def self.get_jobs_data user, jobs, page
		total = jobs.present? ? jobs.count : 0
		jobs = jobs.paginate(:page => page) if page.present? and jobs.present?
		return {jobs: Job.get_data_jobs(user, jobs), total: total}
	end

	def self.get_all_jobs user, page
		jobs = Job.all_jobs user
		Job.get_jobs_data(user, jobs, page)
	end

	def get_company
		self.posted_by.company if self.posted_by.present?
	end

	def self.get_data_jobs user, jobs
		jobs_data = []
		return [] if jobs.nil?
		jobs.each do |job|
			jobs_data.push(job.basic_data(user))
		end
		jobs_data
	end

	def get_applicants_ids
		Rails.cache.fetch("job/applicants/#{self.id}", expires_in: 1.minute) do
			Application.select(:user_id).where(job_id: self.id, removed: false).map(&:user_id)
		end
	end

	def get_all_matchs_ids(application_ids)
		Rails.cache.fetch("job/all_matchs/#{self.id}", expires_in: 1.minute) do
			#Matching.select(:user_id).where(user_id: application_ids, job_id: self.id).map(&:user_id)
			Matching.get_matchings_for_job(self.id).map(&:user_id)
		end
	end

	def get_matchs_ids
		application_ids = self.get_applicants_ids
		matching_ids = self.get_all_matchs_ids(application_ids)
		interview_ids = self.get_accepted_ids
		matching_ids# - interview_ids
	end

	def get_accepted_ids
		Interview.select(:user_id).where(job_id: self.id).map(&:user_id)
	end

	def get_rejected_ids
		Application.select(:user_id).where(job_id: self.id, removed: true).map(&:user_id)
	end

	def self.data_for_filters(user)
		id = user.present? ? user.id : ""
		Rails.cache.fetch("careers_for_filter/#{id}", expires_in: 30.seconds) do
			jobs_data = {}
			careers = []
			looking_for = []
			years_experience = []
			jobs = Job.all_jobs(user)
			careers = CareersJob.where(job_id: jobs.map(&:id)).map(&:career_id)
			jobs_data[:careers] = Career.select(:name, :id).where(id: careers.uniq)
			jobs_data[:years_experience] = jobs.map(&:years_experience).uniq
			jobs_data[:looking_for] = jobs.map(&:looking_for).uniq
			jobs_data
		end
	end

	def get_details user
		user_id = user.present? ? user.id : nil
		data = {
			job: self.basic_data(user),
			apply: Application.user_apply?(user_id, self.id),
			company: self.get_company,
			other_jobs_from_company: Job.get_data_jobs(user, self.posted_by.company.get_published_jobs.where("id != ?", self.id).limit(3)),
			similar_jobs: "TO DO (?)"
		}
		data[:job][:favorite] = Favorite.is_favorite?(user, self.id) if user.present?
		data[:job][:removed] = RemovedJob.is_removed?(user, self.id) if user.present?
		data
	end

	def self.filter_jobs jobs, key_value
		result = jobs.where("jobs.title ILIKE ?", "%#{key_value}%")
	end

	def self.get_all_jobs_by_type looking_for
		Job.where(looking_for: looking_for)
	end

	def self.excel_level
		I18n.t(:excel_level).map { |key, value| [ value, key ] }
	end

	def self.english_level
		I18n.t(:english_level).map { |key, value| [ value, key ] }
	end

	def self.career_status
		I18n.t(:career_status).map { |key, value| [ value, key ] }
	end

	def self.looking_for
		I18n.t(:looking_for).map { |key, value| [ value, key ] }
	end

	def english_level_name
		I18n.t(:english_level)[english_level]
	end

	def excel_level_name
		I18n.t(:excel_level)[excel_level]
	end

	def career_status_name
		I18n.t(:career_status)[career_status]
	end

	def looking_for_name
		I18n.t(:looking_for)[looking_for]
	end

	def company_type_name
		I18n.t(:company_type)[company_type]
	end

	def years_experience_name
		I18n.t(:years_experience)[years_experience]
	end

	def get_vacancies
		return self.vacancies if self.vacancies.present?
		return self.job_expansion.number_vacancies if self.job_expansion.present?
		"-"
	end

	def self.calcule_matchs(newer_than, job_id, user_id)
		# newer_than: máximo tiempo en meses desde que no ha iniciado sesion el usuario.
		# job_id: para que tome todos los procesos abiertos, debe ser nil
		# user_id: si es nil toma todos los usuarios.
		jobs = job_id.present? ? Job.where(id: job_id) : Job.select(:id).where(state: 2)
		jobs.each do |job|
			MatchWorker.perform_async(newer_than, job.id, user_id)
		end
	end

end