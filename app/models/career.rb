class Career < ActiveRecord::Base

	validates :name, presence: true
	has_and_belongs_to_many :accounts
	has_and_belongs_to_many :jobs

  def self.get_careers
    Career.all.order(name: :asc)
  end

  def self.by_country country_id
    Rails.cache.fetch("careers-by-id/#{country_id}", expires_in: 1.minute) do
      Career.where(country: country_id).select(:id, :name, :country).order(name: :asc)
    end
  end

  def self.getCareers(ids)
    Career.where(id: ids)
  end

  def self.getJobsFromCareers(user, ids)
    careers = getCareers(ids)
    careers_jobs = CareersJob.where(career_id: careers.map(&:id))
    Job.all_jobs(user).where(id: careers_jobs.map(&:job_id))    
  end

  def self.for_countries countries_ids
    careers = countries_ids.present? ? Career.where(country: countries_ids) : Career.all
    careers.order(name: :asc)
  end

end