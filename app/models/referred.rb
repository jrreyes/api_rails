class Referred < ActiveRecord::Base

  # == Associations
  belongs_to :job
  belongs_to :company

  validates :name, :last_name, :refered_by, :job_id, :email, presence: true

  def self.add_new_referred(company_user, first_name, last_name, email, job_id, notify)
    referred = Referred.new(name: first_name, last_name: last_name, email: email)
    referred.refered_by = company_user.id 
    referred.company_id = company_user.company_id
    referred.job_id = job_id
    if referred.save
      # send mail to user if notify
      # UserMailer.new_referred(referred).deliver
      true
    else
      p referred.errors.full_messages.to_s
      false
    end
  end

end