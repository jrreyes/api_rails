class Favorite < ActiveRecord::Base
  belongs_to :job
  belongs_to :user

  validates :job_id, :user_id, presence: true

  def self.is_favorite? user, job_id
    return false if user.nil?
    return Favorite.where(user_id: user.id, job_id: job_id, removed: false).exists?
  end

end
