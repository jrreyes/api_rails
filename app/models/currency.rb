class Currency < ActiveRecord::Base

  # == Validations
  validates :name, :symbol, :country_id, presence: true
  validates :name, :symbol, uniqueness: true

  # == Associations
  has_one   :job
  belongs_to :country

  # == Public instance methods
  def to_s
    "#{symbol} - #{name}"
  end
end