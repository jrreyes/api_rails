class Matching < ActiveRecord::Base

  # == Associations
  belongs_to :suggested_by, class_name: 'CompanyUser'
  belongs_to :user
  belongs_to :job

  # == Validations
  validates :user_id, :job_id, presence: true
  validates :user_id, uniqueness: {scope: :job_id}

  # == Scope methods
  scope :not_suggested, -> { where(is_suggested: [false, nil]) }
  scope :accepted, ->(ids) { where("user_id IN (?)", ids) }

  # scope :published, -> { where(published: true) }

  def self.is_match?(user_id, job_id)
    Matching.where(user_id: user_id, job_id: job_id, removed: false).exists?
  end

  def self.get_matchings_for_job job_id
    Rails.cache.fetch("matchs/#{job_id}", expires_in: 1.minute) do
      Matching.joins("INNER JOIN applications ON applications.user_id = matchings.user_id AND matchings.job_id = applications.job_id").where(job_id: job_id, removed: false)
    end
  end

  def self.get_matchings_for_user user_id
    Matching.where(user_id: user_id, removed: false)
  end

  def match_for_job job_id    
  end
  
end