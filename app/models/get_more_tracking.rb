class GetMoreTracking < ActiveRecord::Base
  belongs_to :user
  belongs_to :job

  before_create :set_default
  validates :job_id, :user_id, :source, presence: true

  def self.track_event(user_id, job_id, source, source_utm)
    data = {user_id: user_id, job_id: job_id, source: source, source_utm: source_utm}
    track = GetMoreTracking.where(data)
    if track.exists?
      tracking = track.last
      tracking.count += 1
    else
      tracking = GetMoreTracking.new(data)
    end
    tracking.save
  end

  def set_default
    self.count = 0
  end

end
