class VideoInterview < ActiveRecord::Base

	belongs_to :job
	belongs_to :user
	belongs_to :company_user
	has_many :question_video_interviews, dependent: :destroy

  def self.has_new_video_interview? user_id, job_id
    return VideoInterview.get_interviews.exists?(user_id, job_id, "created")
  end

  def self.get_interviews user_id, job_id, state
    return VideoInterview.where(user_id: user_id, job_id: job_id) if state.nil?
    return VideoInterview.where(user_id: user_id, job_id: job_id, state: state)
  end

  def self.get_questions user_id, job_id
    vi = VideoInterview.get_interviews(user_id, job_id, nil)
    return vi.last.question_video_interviews if vi.exists?
    []
  end

end