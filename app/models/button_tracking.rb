class ButtonTracking < ActiveRecord::Base
  belongs_to :user
  belongs_to :job


  def self.track_event(user_id, job_id, removed, button_type, source)
    tracking = ButtonTracking.new(user_id: user_id, job_id: job_id, removed: removed, button_type: button_type, source: source)
    tracking.save
  end

end
