class Certification < ActiveRecord::Base
  # == Constants
  CERTIFICATION_TYPE = %w(magistrate certified)

  # == Associations
  has_and_belongs_to_many :accounts

  # == Validations
  validates :certified_at, :institution, :name, :certification_type, presence: true
  validates :certification_type, inclusion: { in: CERTIFICATION_TYPE }
end