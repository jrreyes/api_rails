class Answer < ActiveRecord::Base
	belongs_to :application
	belongs_to :question

  validates :application_id, :question_id, :content, presence: true
end
