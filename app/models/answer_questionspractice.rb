class AnswerQuestionspractice < ActiveRecord::Base
  # == Associations
  belongs_to :user
  belongs_to :question_practice

  validates :user_id, :question_practice_id, :answer, presence: true
end