class RequestApplicant < ActiveRecord::Base
  belongs_to :company
  belongs_to :company_user
  belongs_to :job

  def self.set_data(comments, job, company_user)
    request_applicant = RequestApplicant.new
    request_applicant.comments = comments
    request_applicant.job_id = job.id
    request_applicant.company_user_id = company_user.id
    request_applicant.company_id = company_user.company.id
    request_applicant
  end

  def self.requested_company? job_id, company_id
    RequestApplicant.where(job_id: job_id, company_id: company_id).exists?
  end

  def self.is_requested? job_id
    RequestApplicant.where(job_id: job_id).exists?
  end

  def self.comment job_id
    comment = RequestApplicant.where(job_id: job_id).first
    return comment.comments
  end
end
