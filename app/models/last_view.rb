class LastView < ActiveRecord::Base
  belongs_to :job
  belongs_to :company_user

  def self.set_last_view job_id, company_user_id
    data = {job_id: job_id, company_user_id: company_user_id}
    last_view = LastView.where(data)
    unless last_view.exists?
      last_view = LastView.new(data)
    else
      last_view = last_view.last
      last_view.updated_at = Time.now
    end
    last_view.save
  end
end
