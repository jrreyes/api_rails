class CommentsApplication < ActiveRecord::Base
  belongs_to :application
  belongs_to :company_user

  def self.new_comment(content, app_id, company_user_id)
    comment = CommentsApplication.new(content: content, application_id: app_id, company_user_id: company_user_id)
    comment.save
  end

end
