class Message

  # == Public methods
  def initialize(attributes = {})
    @full_name = attributes["full_name"]
    @email = attributes["email"]
    @company_name = attributes["company_name"]
    @position = attributes["position"]
    @phone = attributes["phone"]
    @body = attributes["body"]
  end

  def full_name
    @full_name
  end

  def email
    @email
  end

  def company_name
    @company_name
  end

  def position
    @position
  end

  def phone
    @phone    
  end

  def body
    @body
  end

  def persisted?
    false
  end

  def valid?
    false if @full_name.nil? || @email.nil? || @company_name.nil? || @position.nil? || @phone.nil? || @body.nil?
    true
  end

end