class Account < ActiveRecord::Base

  # == Constatns
  CAREER_STATUS = {
    1 => 'student',
    2 => 'graduate',
    3 => 'qualified'
  }

  ENGLISH_LEVEL = {
    1 => 'basic',
    2 => 'medium',
    3 => 'advanced',
    4 => 'native'
  }

  LOOKING_FOR   = {
    1 => 'practice',
    2 => 'memory',
    3 => 'job',
  }

  EXCEL_LEVEL = {
    0 => 'Not applicable',
    1 => 'Basic',
    2 => 'Medium',
    3 => 'Advanced'
  }

  EMAIL_NOTIFICATION = {
    0 => 'Yes',
    1 => 'No'
  }

  YEARS_EXPERIENCE = {
    1 => 'just_graduated',
    2 => 'one_year',
    3 => 'two_years_or_more'
  }

  ACCOUNT_STATE = {
    0 => 'Active',
    1 => 'Desactivated'
  }

  mount_uploader :avatar, AvatarUploader
  mount_uploader :resume, ResumeUploader
  #store_in_background :resume

  # == Associations

  has_one :newdatauser, dependent: :destroy
  has_one :excel, dependent: :destroy

  belongs_to :user
  belongs_to :country
  belongs_to :university

  has_and_belongs_to_many :languages, join_table: 'account_languages'

  has_and_belongs_to_many :careers
  has_and_belongs_to_many :softwares
  has_and_belongs_to_many :certifications

  # == Constants

  SALARY_PORCENTAGE = 10

  # == Delegates

  #delegate :full_name, to: :user
  delegate :matchings, to: :user
  delegate :matching_jobs, to: :user
  delegate :name, to: :country, prefix: :country, allow_nil: true
  delegate :name, to: :university, prefix: :university, allow_nil: true
  delegate :name, to: :principal_career, prefix: :principal_career, allow_nil: true

  accepts_nested_attributes_for :languages, reject_if: :reject_languages, allow_destroy: true
  accepts_nested_attributes_for :careers, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :certifications, reject_if: :reject_certifications, allow_destroy: true
  accepts_nested_attributes_for :softwares, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :newdatauser, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :excel, reject_if: :all_blank, allow_destroy: true

  before_save :apply_rules
  after_update :update_resume_path, if: proc { |a| a.country_id_changed? && !a.new_record? && !a.old_country_id.blank? }

  # == Validations

  # validates :country_id, :career_status, presence: true, if: :on_personal_info_step?
  # validates :career_status, inclusion: { in: CAREER_STATUS.keys }, if: :on_personal_info_step?

  # validates :english_level, :university_id, :principal_career_id, presence: true, if: :on_carrer_info_step?
  # validates :english_level, inclusion: { in: ENGLISH_LEVEL.keys }, if: :on_carrer_info_step?

  def self.email_notification
    I18n.t(:email_notification).map { |key, value| [ value, key ] }
  end

  def excel_level
    self.excel.level
  end

  def english_level_name
    return "" if self.english_level.nil?
    I18n.t(:english_level)[english_level]
  end

  def excel_level_name
    return "" if self.excel.nil?
    I18n.t(:excel_level)[excel_level]
  end

  def looking_for_name
    return "" if self.looking_for.nil?
    I18n.t(:looking_for)[looking_for]
  end

  def career_status_name
    I18n.t(:career_status)[career_status]
  end

  def years_experience_name
    I18n.t(:years_experience)[years_experience]
  end

  def resume_name
    count_same = User.where(first_name: user.first_name, last_name: user.last_name).count
    if count_same > 1
      "cv_#{user.first_name.strip.downcase}_#{user.last_name.strip.downcase}"+user.id.to_s
    else
      "cv_#{user.first_name.strip.downcase}_#{user.last_name.strip.downcase}"
    end
  end

  def get_career
    return Career.find(self.principal_career_id).name if self.principal_career_id.present?
    ""
  end

  def get_university
    return University.find(self.university_id).name if self.university_id.present?
    ""
  end

  def resume_ok?   
    begin
      return false if resume.url.nil?
      (URI.parse(resume.url).read)
      return true
    rescue OpenURI::HTTPError => e
      return false
    end
  end

  def get_rut
    return self.newdatauser.rut if self.newdatauser.present? && self.newdatauser.rut.present?
    "-"
  end

  def get_phone
    return self.newdatauser.phone if self.newdatauser.present? && self.newdatauser.phone.present?
    "-"
  end

  def apply_rules
    self.looking_for = 3 if self.career_status == 3
    self.years_experience = nil if self.career_status == 1
    self.newdatauser.age_study = nil if self.career_status != 1 && self.newdatauser.present?
  end

  def update_resume_path
    # si el campo está blanco no se hace nada, porque da un error de "objet_not_found" en el S3
    # to do: Verificar algoritmo.
    return if self.resume.blank?
    old_country = Country.find old_country_id
    country     = Country.find country_id
    old_path = "#{old_country.name}"
    new_path = "#{country.name}"
    obj1 = old_path + "/" + "#{resume.filename}"
    obj2 = new_path + "/" + "#{resume.filename}"
    #s3 = AWS::S3.new()
    s3 = Aws::S3::Client.new(region:'sa-east-1')
    #aws_object = s3.buckets['firstjobcvs'].objects[obj1].move_to(obj2)
    aws_object = s3.get_object(bucket: 'firstjobcvs' , key: obj1 ).move_to(obj2)
    #Dir.mkdir(new_path) unless File.exists?(new_path)
    #FileUtils.mv("#{old_path}/#{resume_name}", new_path)    
  end

  def self.search_account_from_process company_user
    jobs = Job.select("id").get_all_jobs_from_company company_user, "todos", "created_at"
    applications = Application.select("user_id").where(job_id: jobs.map(&:id))
    users_ids = applications.map(&:user_id).uniq
    users_ids
  end

  def self.search_accounts(company_user, params)
    @accounts = Account.all
    if params[:search].present?
      users_ids = Account.search_account_from_process company_user if params[:where_search].present? and params[:where_search].to_s == "1"
      @accounts = Account.where(user_id: users_ids) if params[:where_search].to_s == "1"
      career_status = []
      career_status.push(1) if params[:is_student].present?
      career_status.push(2) if params[:is_graduate].present?
      career_status.push(3) if params[:is_qualified].present?
      @accounts = @accounts.where(career_status: career_status) if career_status.present?
      @accounts = @accounts.where("english_level >= ? ", params[:english_level]) if params[:english_level].present?
      @accounts = @accounts.joins(:excel).where("level >= ? ", params[:excel_level]) if params[:excel_level].present?
      @accounts = @accounts.where(country_id: params[:country_ids]) if params[:country_ids].present?
      @accounts = @accounts.where(university_id: params[:universities_ids]) if params[:universities_ids].present?
      @accounts = @accounts.where(principal_career_id: params[:careers_ids]) if params[:careers_ids].present?
      @accounts = @accounts.where(years_experience: params[:years_experience]) if params[:years_experience].present?
      @accounts = @accounts.joins(:newdatauser).where(newdatausers: {age_study: params[:age_study]}) if params[:age_study].present?
      @accounts = @accounts.where('looking_for IN (?)', params[:looking_for]) if params[:looking_for].present?
    end
    @accounts.paginate(:page => params[:page])
  end

  # Hay cosas que se repiten del método anterior. Refactorizar.
  # Status: Beta
  # Este método devuelve todas las cuentas que hacen Match con el Job
  def self.matchings_accounts job_id
    @accounts = Account.all
    @job = Job.find(job_id)
    @accounts = @accounts.where(country_id: @job.countries.map(&:id)) if @job.countries.present?
    @accounts = @accounts.where(career_status: @job.get_career_status) if @job.get_career_status.present? # Usar lo mismo en filtros. Estudiar BD.
    @accounts = @accounts.where(university_id: @job.universities.map(&:id)) if @job.universities.present?
    @accounts = @accounts.where(principal_career_id: @job.careers.map(&:id)) if @job.careers.present?
    @accounts = @accounts.where("english_level >= ? ", @job.english_level) if @job.english_level.present?
    #@accounts = @accounts.joins(:excel).where("level >= ? ", @job.excel_level) if @job.excel_level.present?
    @accounts = @accounts.joins("LEFT OUTER JOIN excels ON excels.account_id = accounts.id")
    .where("excels.level >= ?", @job.excel_level) if @job.excel_level.to_i > 0
    @accounts = @accounts.where("years_experience >= ?", @job.years_experience) if @job.years_experience.present?
    @accounts
  end

  def self.filters accounts, params, job_id
    accounts = accounts.where(country_id: params[:country_id]) if params[:country_id].present?
    accounts = accounts.where(university_id: params[:university_id]) if params[:university_id].present?
    accounts = accounts.where(principal_career_id: params[:career_id]) if params[:career_id].present?
    accounts = accounts.where(career_status: params[:career_status]) if params[:career_status].present?
    accounts = accounts.where(extra_activities: params[:extra_activities]) if params[:extra_activities].present?
    accounts = accounts.where(student_projects: params[:student_projects]) if params[:student_projects].present?
    accounts = accounts.where(student_activities: params[:student_activities]) if params[:student_activities].present?
    accounts = accounts.where("english_level >= ? ", params[:english_level]) if params[:english_level].present?
    accounts = accounts.joins("LEFT OUTER JOIN excels ON excels.account_id = accounts.id")
    .where("excels.level >= ?", params[:excel_level]) if params[:excel_level].to_i > 0
    accounts = accounts.joins(:newdatauser).where(newdatausers: {assistantship: 1}) if params[:assistantship].present?
    accounts = accounts.joins("INNER JOIN matchings ON matchings.user_id = accounts.user_id")
    .where(matchings: {job_id: job_id, removed: false}) if params[:is_match].present?
    return accounts
  end

  def resume_column_to_s
    return "Click para ver" if self.resume.present?
    return "No disponible"
  end

  def years_experience_to_s
    '-' if self.looking_for != 3
    self.try(:years_experience_name).try(:titleize)
  end

end