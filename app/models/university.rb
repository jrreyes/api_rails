class University < ActiveRecord::Base

	# == Associations
	has_many :accounts
	has_and_belongs_to_many :jobs, join_table: 'jobs_universities'


	# == Validations
	validates :name, presence: true

  def self.by_country country_id
    Rails.cache.fetch("universities-by-id/#{country_id}", expires_in: 1.minute) do
      University.where(country_id: country_id).select(:id, :name, :country_id).order(name: :asc)
    end
  end

  def self.for_countries countries_ids
    universities = countries_ids.present? ? University.where( country_id: countries_ids ) : University.all
    universities.order(name: :asc)
  end

  def self.get_universities
    University.all.order(name: :asc)
  end

end