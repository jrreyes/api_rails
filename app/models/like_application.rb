class LikeApplication < ActiveRecord::Base
  belongs_to :application
  belongs_to :company_user

  def self.current_value(application, button_type)
    like_application = LikeApplication.where(application_id: application.id)

    if like_application.exists?
      like_application = like_application.last
      return 1 if like_application.liked == true and button_type == "like_btn"
      return 1 if like_application.liked == false and button_type == "unlike_btn"
    end
    return 0

  end

  def self.likes_click(button, current_company_user, application_id, current_value)
    liked = ''
    like_application = LikeApplication.where(company_user: current_company_user, application_id: application_id )

    if like_application.exists?
      like_application = like_application.last
    else
      like_application = LikeApplication.new(company_user: current_company_user, application_id: application_id)
    end

    if button == "like_btn" && current_value == "0"
      like_application.liked = true
      liked = true
    end
    if button == "unlike_btn" && current_value == "0"
      like_application.liked = false
      liked = false
    end
    if current_value == "1"
      like_application.liked = nil
      liked = ''
    end

    like_application.save
    return liked
  end

end
