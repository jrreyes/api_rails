class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :timeoutable, :async

  # == Friendly url
  extend FriendlyId
  friendly_id :full_name, use: [:slugged, :finders]

  has_one :account, dependent: :destroy

  has_many :applications, dependent: :destroy
  has_many :matchings, dependent: :destroy
  has_many :interviews, dependent: :destroy
  has_many :video_interviews, dependent: :destroy
  has_many :notifications, dependent: :destroy
  has_many :favorites, dependent: :destroy
  has_many :removed_jobs, dependent: :destroy

  accepts_nested_attributes_for :account

  # == Validations
  validates :email, presence: true, uniqueness: true

  before_save :ensure_authentication_token

  # == Public instance methods
  def full_name
  	"#{first_name} #{last_name}"
  end

  def removed_jobs_per_page page
    removed = self.get_removed_jobs
    jobs = Job.where(id: removed.map(&:job_id)).order(created_at: :desc).paginate(:page => page)
    data = {jobs: Job.get_data_jobs(self, jobs), total: self.get_removed_jobs.count}
    return data
  end

  def counter_jobs
    counter_jobs = {}
    counter_jobs[:all_jobs] = Job.public_jobs.count
    counter_jobs[:fav_jobs] = self.user_data_jobs[:fav_jobs].count
    counter_jobs[:apply_jobs] = self.user_data_jobs[:apply_jobs].count
    counter_jobs[:removed_jobs] = self.user_data_jobs[:removed_jobs].count
    counter_jobs
  end

  def all_jobs_user
    all_jobs = Job.public_jobs.map(&:id)
    fav_jobs = self.user_data_jobs[:fav_jobs]
    apply_jobs = self.user_data_jobs[:apply_jobs]
    removed_jobs = self.user_data_jobs[:removed_jobs]
    jobs = all_jobs - fav_jobs - apply_jobs - removed_jobs
    Job.where(id: jobs).order(created_at: :desc)
  end

  def set_user_data new_data_user_params
    self.first_name = new_data_user_params[:first_name]
    self.last_name = new_data_user_params[:last_name]
    self.account = Account.new if self.account.nil?
    self.account.country_id = new_data_user_params[:country_id]
    self.account.newdatauser = Newdatauser.new if self.account.newdatauser.nil?
    self.account.newdatauser.phone = new_data_user_params[:phone]
    self.account.newdatauser.rut = new_data_user_params[:rut_dni]
    self.account.step = "user_info"
  end

  def set_data_academic user_data_academic_params
    self.account = Account.new if self.account.nil?
    self.account.university_id = user_data_academic_params[:university_id]
    self.account.principal_career_id = user_data_academic_params[:principal_career_id]
    self.account.step = "academic_info"
  end

  def set_career_status user_career_status_params
    self.account = Account.new if self.account.nil?    
    self.account.looking_for = user_career_status_params[:looking_for]
    self.account.career_status = user_career_status_params[:career_status]
    self.account.years_experience = user_career_status_params[:years_experience]
    self.account.step = "carrer_info"
    self.account.newdatauser = Newdatauser.new if self.account.newdatauser.nil?
    self.account.newdatauser.age_study = user_career_status_params[:age_study]
  end

  def set_ability user_ability_params
    self.account = Account.new if self.account.nil?
    self.account.english_level =  user_ability_params[:english_level]
    self.account.step = "finished"
    self.account.excel = Excel.new if self.account.excel.nil?
    self.account.excel.level = user_ability_params[:excel_level]
  end

  def set_salary user_salary_params
    self.account = Account.new if self.account.nil?
    self.account.minimum_salary_expected = user_salary_params[:minimum_salary_expected]
    self.account.step = "finished"  
  end

  def resume_pending?
    self.account.resume.url.present? == false
  end

  def set_assistantships params
    self.account.newdatauser.assistantship = params[:users][:assistantship]
    self.account.newdatauser.assistantship_number = params[:users][:assistantship_number].present? ? params[:users][:assistantship_number] : 0
    self.account.newdatauser.qualifications = params[:users][:qualifications]
    self.account.step = "assistantships_info"    
  end

  def set_extra_activities activities_params
    self.account.student_activities = activities_params[:student_activities]
    self.account.sport_activities = activities_params[:sport_activities]
    self.account.student_ind_activities = activities_params[:student_ind_activities]
    self.account.volunteering = activities_params[:volunteering]
    self.account.step = "extra_info"
  end

  def has_resume?
    self.account.resume.path.present?
  end

  def set_answers answers
    ActiveRecord::Base.transaction do
      begin
        answers.each do |answer|
          aq = AnswerQuestionspractice.new
          aq.user_id = self.id
          aq.question_practice_id = answer[:question_id]
          aq.answer = answer[:value]
          aq.text_answer = answer[:value]
          aq.save
        end
        self.account.step = "finished"
      rescue ActiveRecord::StatementInvalid
        return false
      end
      return true
    end    
  end

  def favorites_jobs page
    favorites =  self.get_favorites_jobs
    jobs = Job.where(id: favorites.map(&:job_id)).order(created_at: :desc).paginate(:page => page)
    data = {jobs: Job.get_data_jobs(self, jobs), total: favorites.count}
    return data
  end

  def get_removed_jobs
    self.removed_jobs.where(removed: false)
  end

  def self.get_users_info users
    data = []
    users.each do |user_id|
      user = User.find(user_id)
      user_data = user.user_data
      user_data.delete(:authentication_token) # for security reasons
      data.push(user_data)
    end
    data
  end

  def get_favorites_jobs
    self.favorites.where(removed: false)
  end

  def get_job_answers job_id
    app = Application.user_apply?(self.id, job_id)
    return [] if app.nil?
    app.answers.order(question_id: :asc)
  end

  def get_applications
    self.applications.where("user_removed is NULL")
  end

  def applications_jobs page
    applications = self.get_applications
    jobs = Job.where(id: applications.map(&:job_id)).order(created_at: :desc).paginate(:page => page)
    data = {jobs: Job.get_data_jobs(self, jobs), total: self.applications.count}
    return data
  end

  def user_data_jobs
    data_jobs = {}
    data_jobs[:fav_jobs] = self.get_favorites_jobs.map(&:job_id)
    data_jobs[:apply_jobs] = self.applications.applied_jobs.map(&:job_id)
    data_jobs[:removed_jobs] = self.get_removed_jobs.map(&:job_id)
    data_jobs
  end

  def user_data
    data = {}
    data[:email] = self.email
    data[:authentication_token] = self.authentication_token
    data[:first_name] = self.first_name
    data[:last_name] = self.last_name
    if self.account.present?
      data[:country_id] = self.account.country_id
      data[:university_id] = self.account.university_id
      data[:principal_career_id] = self.account.principal_career_id
      data[:looking_for] = self.account.looking_for
      data[:career_status] = self.account.career_status
      data[:years_experience] = self.account.years_experience
      data[:english_level] = self.account.english_level
      data[:minimum_salary_expected] = self.account.minimum_salary_expected
      data[:excel_level] = self.account.excel.level if self.account.excel.present?
      data[:step] = self.account.step
      data[:resume] = self.account.resume.url
    end
    if self.account.present? and self.account.newdatauser.present? 
      data[:phone] = self.account.newdatauser.phone
      data[:rut_dni] = self.account.newdatauser.rut
      data[:age_study] = self.account.newdatauser.age_study
      data[:assistantship] = self.account.newdatauser.assistantship
      data[:assistantship_number] = self.account.newdatauser.assistantship_number
      data[:qualifications] = self.account.newdatauser.qualifications
      data[:student_activities] = self.account.student_activities
      data[:sport_activities] = self.account.sport_activities
      data[:student_ind_activities] = self.account.student_ind_activities
      data[:volunteering] = self.account.volunteering
    end
    data
  end

  private

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def self.from_access_token_facebook(oauth_access_token, public_token)

    @graph = Koala::Facebook::API.new(oauth_access_token)
    profile = @graph.get_object("me?fields=name,email, first_name, last_name")

    email = profile["email"]
    provider = "facebook"

    user = User.where(email: email)

    if user.exists?
      user = user.last
      user.public_token = public_token if public_token.present? and user.public_token.nil?
      user.save
      return user
    end

    return nil if email.nil? # Si el usuario no comparte su email ¬¬    

    where(email: email).first_or_create do |user|
      user.provider = provider
      user.uid = profile["id"]
      user.first_name = profile["first_name"]
      user.last_name = profile["last_name"]
      user.email = profile["email"]
      user.password = Devise.friendly_token[0,20]
      user.public_token = public_token if public_token.present?
    end

  end

  # Overwrite devise, password may not required if it's an existing record
  def password_required?
    new_record? ? super : false
  end

end