class Country < ActiveRecord::Base

	validates :name, presence: true

	has_many :accounts
	has_and_belongs_to_many :jobs, join_table: 'jobs_countries'

  def self.get_countries
    Country.all.order(name: :asc)
  end

end