class Language < ActiveRecord::Base

  # == Associations

  has_and_belongs_to_many :accounts, join_table: 'account_languages'

  # == Validations
  validates :level, :name, presence: true
end