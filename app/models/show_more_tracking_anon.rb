class ShowMoreTrackingAnon < ActiveRecord::Base
  belongs_to :job

  before_create :set_default
  validates :job_id, :source, presence: true

  def self.track_event(public_token, job_id, source, source_utm)
    data = {public_token: public_token, job_id: job_id, source: source, source_utm: source_utm}
    track = ShowMoreTrackingAnon.where(data)
    if track.exists?
      tracking = track.last
      tracking.count += 1
    else
      tracking = ShowMoreTrackingAnon.new(data)
    end
    tracking.save
  end

  def set_default
    self.count = 0
  end


end
