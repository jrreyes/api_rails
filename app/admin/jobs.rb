ActiveAdmin.register Job do
  config.batch_actions = false

  permit_params :title, :company_id, :english_level, :career_status, :looking_for, :maximum_salary_offered, :published, :maximum_salary_offered, :show_salary,
  :location, :excel_level, :description, :posted_by_id, :hierarchy_job

  belongs_to :company

  scope :all
  scope :published
  scope :unpublished

  filter :company
  filter :location
  filter :english_level, as: :select, collection: proc { Job.english_level }
  filter :career_status, as: :select, collection: proc {  Job.career_status }
  filter :looking_for, as: :select, collection: proc { Job.looking_for }
  filter :published

  index do
    column('Job Image'){ |job| image_tag(job.avatar.thumb)  if job.avatar.present? }
    column :posted_by
    column :company
    column :title
    #column :description do |a| a.description[0..80] + "....."  end
    #column :location
    column :english_level_name
    column :excel_level_name
    column :career_status do |a|
      Job::CAREER_STATUS.map{ |v,k| k if v==a.career_status}.compact
    end
    #column('career_status') do |a|
      #Account::CAREER_STATUS.map{ |v,k| k if v==a.career_status}.compact
    #end
    column :looking_for_name
    #column :maximum_salary_offered
    #column :show_salary
    column :published
    column :created_at
    #column :updated_at

    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :avatar, as: :file, hint: f.template.image_tag(job.avatar, size: "70x55")
      #f.input :posted_by, input_html: { style: 'width: 200px', disabled: true }
      f.input :company, input_html: { style: 'width: 200px', disabled: true }
      f.input :title, input_html: { style: 'width: 400px' }
      f.input :description, input_html: { style: 'width: 400px' }
      f.input :location, input_html: { style: 'width: 400px' }
      f.input :english_level, as: :select, collection: Job.english_level, include_blank: false
      f.input :career_status, as: :select, collection: Job.career_status, include_blank: false
      f.input :looking_for, as: :select, collection: Job.looking_for, include_blank: false
      f.input :maximum_salary_offered, input_html: { style: 'width: 100px' }
      f.input :show_salary
      f.input :published
      f.input :excel_level, as: :select, collection: Job.excel_level, include_blank: false
      f.input :hierarchy_job, input_html: { style: 'width: 100px' }
      @job =  Job.where( slug: params[:id] ).first
      f.input :posted_by_id, as: :select, include_blank: false, input_html: { style: "width:300px" }, collection: CompanyUser.where( company_id: @job.company_id ).sort.map{ |c| [c.email, c.id ] }
    end
    f.actions
  end

  show do |job|
    attributes_table do
      row('Job Image'){ |job| image_tag(job.avatar, size: "70x55") }
      row :posted_by
      row :company
      row :title
      row :description
      row :location
      row('English Level') { job.english_level_name }
      row('Career Status') { job.career_status_name }
      row('Looking For')   { job.looking_for_name   }
      row :maximum_salary_offered
      row :show_salary
      row :published
      row :hierarchy_job
      row :updated_at
      row :created_at
    end
  end

  controller do
    def update
      update! do |format|
        format.html { redirect_to admin_company_jobs_path(@company) }
      end
    end
  end
end