ActiveAdmin.register User do
  config.batch_actions = false

  permit_params :id, :first_name, :last_name, :email, :born_date, :password, :password_confirmation, :account,
   account_attributes: [:avatar, :resume, :country_id, :id,  :principal_career_id, :university_id, :english_level, :career_status, :looking_for, :years_experience,
    :minimum_salary_expected, :email_notification, :account_state, :count_desactivated, :step, :satander_user, newdatauser_attributes: [:rut, :phone, :age_study, :id],
        excel_attributes: [:level, :id]]


  filter :id  
  filter :first_name
  filter :last_name
  filter :email
  filter :sign_in_count
  filter :created_at

  index do
    column :id
    column :first_name
    column :last_name
    column :email
    column :born_date
    column :current_sign_in_at
    column :last_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :email
      row :born_date
      row :updated_at
      row :created_at
    end
  end

  #form do |f|
  #  f.inputs "Details" do
  #    f.input :email
  #  end
  #  f.actions
  #end

  form partial: 'form'
end
