ActiveAdmin.register Currency do
  config.batch_actions = false
  filter :country
  filter :name, as: :select, collection: proc { Currency.order('symbol').map{|c| [c, c.name]} }
  config.sort_order = "country_id_asc"

  permit_params :country_id, :name, :symbol

  index do
    column :country
    column :name
    column :symbol
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :country
      row :name
      row :symbol
      row :created_at
      row :updated_at
    end
  end
end