ActiveAdmin.register CompanyUser do
  config.batch_actions = false
  belongs_to :company, parent_class: Company

  permit_params :first_name, :last_name, :email, :password, :password_confirmation, :hierarchy, :company_id

  filter :company
  filter :first_name
  filter :last_name
  filter :email
  filter :created_at

  index do
    column :company
    column :first_name
    column :last_name
    column :email
    column :hierarchy
    column :sign_in_count
    column :last_sign_in_at
    #column :confirmed_at
    column :created_at
    #column :updated_at
    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :first_name, input_html: { style: "width: 350px" }
      f.input :last_name, input_html: { style: "width: 350px" }
      f.input :email, input_html: { style: "width: 350px" }
      f.input :password, input_html: { style: "width: 350px" }
      f.input :password_confirmation, input_html: { style: "width: 350px" }
      f.input :hierarchy, as: :select, include_blank: false , input_html: { style: "width:120px" } ,collection: CompanyUser::HIERARCHY.sort.map{ |k, v| [v.titleize, k] }
    end
    f.actions
  end

  show do |company_user|
    attributes_table do
      row :company
      row :first_name
      row :last_name
      row :email
      row :hierarchy
    end
  end

  controller do
    def create
      build_resource
      resource.skip_confirmation!
      create!
    end
  end

end