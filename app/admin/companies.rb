ActiveAdmin.register Company do
  config.batch_actions = false

  permit_params :name, :description, :company_status, :founded, :cant_employees, :video_source_url, :avatar, :page_url, :public_image, :slider_image, 
  :industry, :central_office, :img_share_fb, :square_image, :cover_image

  filter :name
  filter :created_at
  #filter :company_status, as: :select,  collection: Company::COMPANY_STATUS.sort_map{ |k,v| [v.titleize, k| } 

  index do
    column :id
    column('Image'){ |company| image_tag(company.avatar, size: "70x55") }
    #column('Public Image'){ |company| image_tag(company.public_image, size: "70x55") }
    #column('Slider Image'){ |company| image_tag(company.slider_image, size: "70x55") }
    column :name
    column :description do |a| a.description[0..100] + "......"  end
    column :created_at
    #column :updated_at
    column('company_status') do |a| 
      Company::COMPANY_STATUS.map{ |v,k| k if v==a.company_status}.compact
    end
    #column('company_status'){ |a| a.company_status }
    #column :company_status
    column "Company Users" do |company|
      link_to "View Users", admin_company_company_users_path(company), class: 'commit button'
    end
    column "Company Interviews" do |company|
      #link_to "View Interviews", admin_company_interviews_path(company), class: 'commit button'
    end
    actions
  end

  form do |f|
    f.inputs "Details" do
      f.input :avatar, as: :file, hint: f.template.image_tag(company.avatar, size: "70x55")
      f.input :square_image, as: :file, hint: f.template.image_tag(company.square_image, size: "300x300")
      f.input :cover_image, as: :file, hint: f.template.image_tag(company.cover_image, size: "600x315")
      f.input :public_image, as: :file, hint: f.template.image_tag(company.public_image, size: "669x157")
      f.input :slider_image, as: :file, hint: f.template.image_tag(company.slider_image, size: "241x190")
      f.input :img_share_fb, as: :file, hint: f.template.image_tag(f.object.img_share_fb.url, size: "700x300")
      f.input :name
      f.input :description
      f.input :company_status, as: :select, include_blank: false , input_html: { style: "width:100px" } ,collection: Company::COMPANY_STATUS.sort.map{ |k, v| [v.titleize, k] }
      f.input :founded, label: "Fundada", input_html: { style: "width:90px", placeholder: "Ejemplo: 2013" }
      f.input :cant_employees,label: "Cantidad de Empleados", input_html: { style: "width:90px", placeholder: "Ejemplo: 1.985" }
      f.input :industry, label: "Industria", input_html: { style: "width:90px", placeholder: "Ejemplo: Finanzas" }
      f.input :central_office, label: "Oficina Central" , input_html: { style: "width:90px", placeholder: "Ejemplo: Chile" }
    end
    f.actions
  end

  show do
    attributes_table do
      row('Image'){ |company| image_tag(company.avatar, size: "70x55") }
      row('Square'){ |company| image_tag(company.square_image, size: "300x300") }
      row('Cover'){ |company| image_tag(company.cover_image, size: "600x315") }
      row('Public Image'){ |company| image_tag(company.public_image, size: "669x157") }
      row('Slider Image'){ |company| image_tag(company.slider_image, size: "241x190") }
      row :name
      row :description
      #row :updated_at
      #row :created_at
      row :founded
      row :cant_employees
      row :industry
      row :central_office
    end
  end
end