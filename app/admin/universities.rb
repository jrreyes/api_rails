ActiveAdmin.register University do
  config.batch_actions = false

    permit_params :name, :country_id
  
  filter :name
  filter :country_id
  filter :id

  index do
    column :id
    column :name
    column :created_at
    column :updated_at
    column :country_id
    actions
  end
  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :country_id, as: :select, include_blank: false , input_html: {style: "width:300px" } ,collection: Country.all.sort.map{ |c| [c.name.titleize, c.id] }
    end
    f.actions
  end
end
