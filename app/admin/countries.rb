ActiveAdmin.register Country, as: 'Countries' do
  config.batch_actions = false
  #filter :name, as: :select, collection: proc { Country.order.map{|c| [c.name, c.name]} }
  config.sort_order = "name_asc"

  permit_params :name, :calling_code, :iso_code

  filter :name

  index do
    column :id
    column :name
    column :calling_code
    column :iso_code
    column :created_at
    column :updated_at
    actions
  end

  show do
    attributes_table do
      row :name
      row :calling_code
      row :iso_code
      row :created_at
      row :updated_at
    end
  end
end