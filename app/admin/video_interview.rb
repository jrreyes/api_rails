ActiveAdmin.register  VideoInterview do
  permit_params :job_id, :user_id, :company_user_id, :state, :viewed, :link
end