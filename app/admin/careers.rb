ActiveAdmin.register Career do
  config.batch_actions = false

  permit_params :name, :country

  index do
    column :id
    column :name
    column :country
    column :created_at
    actions
  end
  form do |f|
    f.inputs "Details" do
      f.input :name
      f.input :country, as: :select, include_blank: false , input_html: {style: "width:300px" } ,collection: Country.all.sort.map{ |c| [c.name.titleize, c.id] }
    end
    f.actions
  end
end