Rails.application.routes.draw do

  root to:'home#index', as: "root"
  require 'sidekiq/web'
  authenticate :admin_user do
    mount Sidekiq::Web => '/sidekiq'
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :company_users, controllers: { sessions: 'client/sessions' }
  devise_for :users, controllers: { sessions: 'users/sessions', registrations: 'users/registrations', passwords: "users/passwords"}

  get "auth/facebook/callback", to: "users/facebook#facebook", as: :get_user_from_token  

  # CURRENT USER
  get "users/:id", to: "users/user#show", as: :show_user
  get "me", to: "users/user#me", as: :show_me
  get "user/favorites", to: "users/user#get_favorites", as: :get_favorites
  get "user/applications", to: "users/user#get_applications", as: :get_applications
  get "user/removed", to: "users/user#get_removed", as: :get_removed
  get "user/counter-jobs-user", to: "users/user#counter_jobs_user", as: :counter_jobs_user
  get "user/applications/:job_id", to: "users/user#get_job_application", as: :get_job_application
  get "user/answers/:job_id", to: "users/user#get_job_answers", as: :get_job_answers
  post "user/cv-upload", to: "users/user#upload_resume", as: :upload_resume
  post "/job/remove", to: "users/user#job_remove", as: :job_remove
  post "/job/favorite", to: "users/user#job_favorite", as: :job_favorite

  # USERS
  get "/users/:id/resume", to: "users/user#download_resume", as: :resume_user
  post "/users/new-user-data", to: "users/user#new_user_data", as: :new_user_data
  post "/users/user-data-academic", to: "users/user#user_data_academic", as: :user_data_academic
  post "/users/user-career-status", to: "users/user#user_career_status", as: :user_career_status
  post "/users/user-ability", to: "users/user#user_ability", as: :user_ability
  post "/users/user-salary", to: "users/user#user_salary", as: :user_salary
  post "/users/user-assistantships", to: "users/user#user_assistantships", as: :user_assistantships
  post "/questions", to: "users/user#set_user_answers", as: :set_answers
  post "/users/user-extra-activities", to: "users/user#user_extra_activities", as: :user_extra_activities
  get "download/resume" => "client/process#download_resume", as: :download_resume

  # SEED FORM DATA
  get "/countries", to: "data#get_countries", as: :get_countries
  get "/careers", to: "data#get_careers", as: :get_careers
  get "/careers/:country_id", to: "data#get_careers_by_country", as: :get_careers_by_country
  get "/universities", to: "data#get_universities", as: :get_universities
  get "/universities/:country_id", to: "data#get_universities_by_country", as: :get_universities_by_country
  get "/questions", to: "data#get_questions", as: :get_questions

  # JOBS
  get "jobs", to: "users/jobs#index", as: :jobs
  get "jobs/:id", to: "users/jobs#show", as: :job
  get "job/data-for-filters", to: "users/jobs#data_for_filters"
  post "/job/apply", to: "users/jobs#job_apply", as: :job_apply
  delete "job/apply/:job_id", to: "users/jobs#job_apply_undo", as: :job_apply_undo
  get "jobs/:id/questions", to: "users/jobs#get_job_questions", as: :get_job_questions
  post "job/answer-job-questions", to: "users/jobs#answers_job_questions", as: :answers_job_questions
  get "job/filter", to: "users/jobs#filter_jobs"
  post "job/get-filtered-jobs", to: "users/jobs#get_filtered_jobs"
  post "job/button", to: "users/jobs#button_click_tracking"
  post "job/share", to: "users/jobs#share_click_tracking"

  ##  CLIENT

  get "empresas", to: "client/company#index", as: :client_index
  get "ingresar", to: "client/company#login", as: :client_login
  get 'empresas', to: "client/company#index", as: :root_companies
  get "productos" => "client/company#products", as: :home_products
  get "clientes" => "client/company#clients", as: :home_clients
  get "planes" => "client/company#plans", as: :home_plans
  get "contacto" => "client/company#contact", as: :home_contact
  get "publicar" => "client/company#publish", as: :home_publish
  get "ingresar" => "client/company#login", as: :home_login
  get "employer-branding" => "client/company#employer_branding", as: :employer_branding
  get "select-company", to: 'client/company#select_company', as: :select_company
  post "set-company", to: 'client/company#set_company_for_kam', as: :set_company_for_kam
  post "post_requests", to: "client/company#post_requests", as: :post_requests
  resources :post_requests, only: [:new, :create]

  # Contact form
  #get 'contact' => 'client/company#new', as: 'contact'
  post 'contact' => 'client/company#create', as: 'contact'
  ################################################################
  namespace :client do
    get "process", to: "process#index", as: :process_index
    get "process/:job_id", to: "process#show", as: :process_show
    get "process/:job_id/getexcel", to: "process#getexcel", as: :getexcel
    post "process/:job_id/getexcel", to: "process#getexcel", as: :getexcel_selected
    get "process/:job_id/getresumes", to: "process#get_job_resumes", as: :get_job_resumes
    post "process/:job_id/getresumes", to: "process#get_job_resumes", as: :get_job_resumes_selected
    post "process/:job_id/republish", to: "process#republish", as: :republish_job

    get "get-application-answers/:id", to: "process#get_application_answers", as: :get_application_answers
    get "get-application-comments/:id", to: "process#get_application_comments", as: :get_application_comments
    get "job/:job_id", to: "process#get_job_details"
    get "users/:user_id/resume", to: "process#get_user_resume", as: :get_user_resume
    get "users/:user_id/display-resume", to: "process#display_resume", as: :display_resume
    get "users/:user_id/check_resume", to: "process#check_resume", as: :check_resume
    get "job/:job_id/applicants", to: "process#get_applicants_for_job"
    post "process/request_candidates", to: "process#request_candidates", as: :request_candidates
    post "process/set_job_state", to: "process#set_job_state", as: :set_job_state
    post "users/like", to: "process#likes_buttons", as: :likes_buttons


    ################################################################
    get "users/:user_id/profile", to: "process#get_profile_partial", as: :get_profile_partial
    get "users/:user_id/process", to: "process#get_process_partial", as: :get_process_partial
    get "users/:user_id/answers", to: "process#get_answers_partial", as: :get_answers_partial
    get "users/:user_id/comments", to: "process#get_comments_partial", as: :get_comments_partial
    post "users/:user_id/new-comment", to: "process#new_application_comment", as: :new_application_comment
    post "users/delete_comment/:comment_id", to: "process#delete_application_comment", as: :delete_application_comment
    post "users/add_applicant", to: "process#add_applicant", as: :add_applicant
    post "users/edit_comment", to: "process#edit_application_comment", as: :edit_application_comment
    ################################################################
    get "publish/new", to: "publish#process_step_1", as: :new_process
    post "publish/create_process", to: "publish#create_process", as: :create_process

    get "publish/:job_id/step1", to: "publish#edit_step_1", as: :edit_step_1
    patch "publish/step1_values", to: "publish#process_step_1_update", as: :process_step_1_update

    get "publish/:job_id/step2", to: "publish#process_step_2", as: :process_step_2
    patch "publish/step2_values", to: "publish#process_step_2_update", as: :process_step_2_update

    get "publish/:job_id/step3", to: "publish#process_step_3", as: :process_step_3
    patch "publish/step3_values", to: "publish#process_step_3_update", as: :process_step_3_update

    get "publish/:job_id/step4", to: "publish#process_step_4", as: :process_step_4
    post "publish/:job_id/add-question", to: "publish#add_question", as: :add_question
    post "publish/delete-question", to: "publish#delete_question", as: :delete_question
    post "publish/update-question", to: "publish#update_question", as: :update_question
    patch "publish/step4_values", to: "publish#process_step_4_update", as: :process_step_4_update

    get "publish/add_new_question", to: "publish#add_new_question", as: :add_new_question

    get "publish/:job_id/step5", to: "publish#process_step_5", as: :process_step_5
    patch "publish/step5_values", to: "publish#process_step_5_update", as: :process_step_5_update
    get "publish/:job_id/step6", to: "publish#process_step_6", as: :process_step_6
    ################################################################
    get "configuration", to: "configuration#index", as: :configuration
    get "insigths", to: "insigths#index", as: :insigths
    get "interviews", to: "interviews#index", as: :interviews
    ################################################################
    get "search", to: "search#index", as: :search
    post "/data-for-countries", to: "search#data_for_countries", as: :data_for_countries

  end

end
