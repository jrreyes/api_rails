set :stage, :production
set :rails_env, :production

server '166.78.142.219', user: 'root', roles: %w{app}
role :app, %w{root@166.78.142.219}, :primary => true