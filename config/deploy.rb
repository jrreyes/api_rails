# config valid only for current version of Capistrano
lock '3.5.0'

set :application, 'firstjob_server'
set :repo_url, 'git@bitbucket.org:firstjob/firstjob_server.git'
set :stages, ["staging", "production"]
set :default_stage, "production"
set :branch, 'deploy'

set :user, 'root'                           # ssh login user
set :domain, '166.78.142.219'
set :deploy_to, "/home/ubuntu/firstjob_server"
set :local_repository,  "/home/ubuntu/repository"
set :scm, "git"
role :app, %w{root@166.78.142.219}, :primary => true #This may be the same as your `Web` server
# role :db, %w{root@23.253.233.65}
set :deploy_via, :export

set :pty, :true

set :git_shallow_clone, 1
set :scm_verbose, true

set :use_sudo, true                        # if turn on, all capistrano maked dir would be of root
set :sudo_prompt, ""

#set :bundle_cmd, 'source $HOME/.bash_profile && bundle'

namespace :deploy do

  desc "migration"
  task :migration do
    on roles(:app) do
      execute "cd #{release_path}; bundle exec rake db:migrate RAILS_ENV=production"
    end
  end

  desc "precompile"
  task :precompile do
    on roles(:app) do
      execute "cd #{release_path}; bundle exec rake assets:precompile RAILS_ENV=production"
    end
  end

  desc 'Restart nginx'
  task :restart_nginx do
    on roles(:app) do
      execute "sudo service nginx restart"
    end 
  end

  desc 'Restart unicorn'
  task :restart_unicorn do
    on roles(:app) do
      execute "sudo service unicorn restart"
    end 
  end

  desc 'Start sidekiq'
  task :start_sidekiq do
    on roles(:app) do
      execute "sleep 5" #esperar que se levante nginx y unicorn
      execute "cd #{release_path}; bundle exec sidekiq -d -e production -L log/sidekiq_production.log"
    end
  end


  after :deploy, "deploy:migration"
  after :deploy, "deploy:precompile"
  after :deploy, "deploy:restart_unicorn"
  after :deploy, "deploy:restart_nginx"
  after :deploy, "deploy:start_sidekiq"

end