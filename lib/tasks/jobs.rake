namespace :job do
  desc "Update state job values"
  task :update_job_states => :environment do

    # state = 1 => Proceso en borrador
    # state = 2 => Proceso en curso y público
    # state = 3 => Proceso en curso y en pausa (no público)
    # state = 4 => Proceso cerrado.

    jobs = Job.all
    total = jobs.count
    i = 1
    jobs.each do |job|
      p "Update state: " + job.id.to_s + " - (#{i}/#{total})"
      job.state = 2 if job.published.present?
      job.state = 4 if job.published.nil? || job.published == false
      job.save
      i += 1
    end

  end

end